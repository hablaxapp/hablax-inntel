package com.inntel.inntel_recargas;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.inntel.inntel_recargas.adapters.TopUpAdapter;
import com.inntel.inntel_recargas.mail.NautaMailReceiver;
import com.inntel.inntel_recargas.mail.NautaMailSender;
import com.inntel.inntel_recargas.models.Topup;
import com.inntel.inntel_recargas.recivers.NetworkChangeReceiver;
import com.inntel.inntel_recargas.responses.Phone;
import com.inntel.inntel_recargas.responses.ResponseBalance;
import com.inntel.inntel_recargas.responses.ResponseCancel;
import com.inntel.inntel_recargas.responses.ResponseStatus;
import com.inntel.inntel_recargas.responses.ResponseTopUp;
import com.inntel.inntel_recargas.services.ImapPushService;
import com.inntel.inntel_recargas.utils.ConnectionHelper;
import com.inntel.inntel_recargas.utils.Constants;
import com.inntel.inntel_recargas.utils.Crypto;
import com.inntel.inntel_recargas.utils.JSONHelper;
import com.orm.SchemaGenerator;
import com.orm.SugarContext;
import com.orm.SugarDb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static com.inntel.inntel_recargas.R.id.action_report;
import static com.inntel.inntel_recargas.R.string.Dismiss;
import static com.inntel.inntel_recargas.R.string.InactiveDataRecv;

public class HistoryActivity extends AppCompatActivity {

    public final String TAG = "HistoryActivity";

    private static final int PERMISSION_WRITE_FILE = 7556;

    public ListView topUps;
    public CardView ribbon;
    public EditText search;
    public String nautaUser;
    public String nautaPass;
    public String inntelToken;
    public TextView tvRecargadas;
    public TextView tvErrores;
    public TextView tvPendientes;
    public static TextView tvUnsend;


    private ProgressDialog progessDialog;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //inicializa la DDBB y la crea si no existe
        SugarContext.init(getApplication());
        SchemaGenerator schemaGenerator = new SchemaGenerator(this);
        schemaGenerator.createDatabase(new SugarDb(this).getDB());

        setContentView(R.layout.activity_history);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tvRecargadas = (TextView) findViewById(R.id.tvRecargadas);
        tvErrores = (TextView) findViewById(R.id.tvErrores);
        tvPendientes = (TextView) findViewById(R.id.tvPendientes);
        tvUnsend = (TextView) findViewById(R.id.tvUnsend);

        search = (EditText) findViewById(R.id.EditTextSearch);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().equals(""))
                    toolbar.setTitle(String.format("Historial"));
                updateTopUpList(filterTopups(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        loadCredentials();

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ConnectionHelper.connect(HistoryActivity.this, new ConnectionHelper.ConnectListener() {
                    @Override
                    public void connectedOK(NetworkChangeReceiver.ConnectStatus status) {

                        new NautaReader().execute();
                        search.setText("");
                        updateTopUpList(getAllTopups());


                    }

                    @Override
                    public void connectedBLocked() {
//                        Toast.makeText(MainActivity.this, "Debe estar conectado para poder enviar o recibir.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void connectedFail(Exception e, String error) {
                        Snackbar.make(getCurrentFocus(), getString(InactiveDataRecv), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getString(Dismiss), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                    }
                                }).show();
                    }
                });

            }
        });
        IconDrawable iconSend = new IconDrawable(this, FontAwesomeIcons.fa_refresh);
        fab.setImageDrawable(iconSend.color(Color.WHITE));

        ActionBar bar = getSupportActionBar();
        bar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_TITLE);
        bar.setHomeButtonEnabled(true);

        ribbon = (CardView) findViewById(R.id.ribbon);
        setRibbonVisibility();

        topUps = (ListView) findViewById(R.id.listViewTopUps);
        updateTopUpList(getAllTopups());
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        topUps.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == 1) {
                    fab.hide();
                } else {
                    fab.show();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                //nothing to do here
            }
        });
    }

    public void setRibbonVisibility() {
        if (getUnverifiedActions(this) > 0) {
            TextView ribbonText = (TextView) findViewById(R.id.ribbonText);
            ribbonText.setText(getString(R.string.UnverifiedActions) + " " + String.valueOf(getUnverifiedActions(this)));
            ribbon.setVisibility(View.GONE);
        } else {
            ribbon.setVisibility(View.GONE);
        }
    }

    public void cleanNewStatus() {
        for (Topup topup : Topup.find(Topup.class, "isnew = ?", "1")) {
            topup.setNew(false);
            topup.save();
        }
    }

    public static synchronized int getUnverifiedActions(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("QvaTel_recargas", MODE_PRIVATE);
        return preferences.getInt("unverified_actions", 0);
    }

    public static synchronized void setUnverifiedActions(Context context, int actions) {
        SharedPreferences preferences = context.getSharedPreferences("QvaTel_recargas", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        if (actions < 0) actions = 0;
        editor.putInt("unverified_actions", actions);
        editor.commit();
    }

    public void loadCredentials() {
        SharedPreferences preferences = getSharedPreferences("QvaTel_recargas", MODE_PRIVATE);
        nautaUser = preferences.getString("user", "");
        nautaPass = preferences.getString("pass", "");
        inntelToken = preferences.getString("token", "");
    }

    public List<Topup> getAllTopups() {
//        return Topup.listAll(Topup.class, "date desc");
        return Topup.find(Topup.class, "status != ?", new String[]{"Sin Verificar"}, null, "date desc", null);
    }

    public List<Topup> filterTopups(String filter) {
        String[] whereArgs = {"%" + filter + "%", "%" + filter + "%", "Sin Verificar"};
        return Topup.find(Topup.class, "(phone LIKE ? OR reference LIKE ?) AND status != ?", whereArgs, null, "date desc", null);
    }

    public void updateTopUpList(List<Topup> topups) {
        int crecargadas = 0;
        int cerrores = 0;
        int cpendientes = 0;
        int cnotsend = 0;
        Topup[] listItems = new Topup[topups.size()];
        for (int i = 0; i < topups.size(); i++) {
            listItems[i] = topups.get(i);
            if (listItems[i].getStatus().equals("Recargado")) {
                crecargadas++;
            } else if (listItems[i].getStatus().equals("Fallida")) {
                cerrores++;
            } else if (listItems[i].getStatus().equals("Pendiente")) {
                cnotsend++;
            } else {
                cpendientes++;
            }
        }
        ArrayAdapter adapter = new TopUpAdapter(this, listItems);
        topUps.setAdapter(adapter);
        tvRecargadas.setText(String.valueOf(crecargadas));
        tvErrores.setText(String.valueOf(cerrores));
        tvPendientes.setText(String.valueOf(cpendientes));
        tvUnsend.setText(String.valueOf(cnotsend));
        toolbar.setTitle(String.format("Historial (%s)", crecargadas + ""));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_history, menu);

        menu.findItem(R.id.action_clean_history).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_trash)
                        .colorRes(R.color.colorAccent)
                        .actionBarSize());

        menu.findItem(R.id.action_credits).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_dollar)
                        .colorRes(R.color.colorAccent)
                        .actionBarSize());

        menu.findItem(R.id.action_savecsv).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_save)
                        .colorRes(R.color.colorAccent)
                        .actionBarSize());

        menu.findItem(action_report).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_refresh)
                        .colorRes(R.color.colorAccent)
                        .actionBarSize());

        return true;
    }

    /*
    * Habilitar los icons en el menú si están disponibles
    * */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (menu != null) {
            if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
                try {
                    Method m = menu.getClass().getDeclaredMethod(
                            "setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                } catch (NoSuchMethodException e) {
                } catch (Exception e) {
                }
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.action_clean_history) {
            showCleanHistoryAlert();
            return true;
        } else if (id == R.id.action_credits) {
            Intent creditsActivity = new Intent(this, CreditsActivity.class);
            startActivity(creditsActivity);
            return true;
        } else if (id == R.id.action_savecsv) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_WRITE_FILE);
            } else {
                saveCSV();
            }
            return true;
        } else if (action_report == id) {
            ConnectionHelper.connect(this, new ConnectionHelper.ConnectListener() {
                @Override
                public void connectedOK(NetworkChangeReceiver.ConnectStatus status) {
                    Date date = new Date();
                    Map<String, String> map = new TreeMap<String, String>();
                    map.put("action", "getReport");
                    map.put("token", App.inntelToken);
                    map.put("date", date.getTime() + "");
                    try {
                        map.put("code", Crypto.getSHA1(App.inntelToken + String.valueOf(date.getTime())));
                        Gson localGson = new Gson();

                        StatusSenderTask task = new StatusSenderTask();

                        task.view = HistoryActivity.this.getCurrentFocus();
                        task.command = "getReport";
                        task.body = localGson.toJson(map);
                        task.execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void connectedBLocked() {

                }

                @Override
                public void connectedFail(Exception e, String error) {
                    try {
                        Snackbar.make(HistoryActivity.this.getCurrentFocus(), HistoryActivity.this.getString(R.string.InactiveDataRecv), -2).setAction(HistoryActivity.this.getString(Dismiss), new View.OnClickListener() {
                            public void onClick(View view) {
                            }
                        }).show();
                        return;
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            });

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_WRITE_FILE) {
            saveCSV();
        }
    }

    public void saveCSV() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        File inntel_folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/QvaTel");
        if (!inntel_folder.exists()) inntel_folder.mkdirs();
        final File file = new File(inntel_folder, "recargas_" + sdf.format(new Date()) + ".csv");
        try {
            FileOutputStream fo = new FileOutputStream(file);
            fo.write("Número, Estado, Fecha, Referencia\n".getBytes());
            for (Topup topup : Topup.listAll(Topup.class, "date desc")) {
                fo.write((topup.getPhone() + "," + topup.getStatus() + "," + sdf.format(topup.getDate()) + "," + topup.getReference() + "\n").getBytes());
            }
            fo.close();

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.FileSaved) + " " + file.getAbsolutePath());
            builder.setCancelable(true)
                    .setPositiveButton(getString(R.string.Open),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent();
                                    intent.setAction(android.content.Intent.ACTION_VIEW);
                                    intent.setDataAndType(Uri.fromFile(file), "text/csv");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                            })
                    .setNegativeButton(getString(R.string.Ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
            AlertDialog alert = builder.create();
            alert.setTitle(getString(R.string.FileSavedOk));
            alert.show();
        } catch (FileNotFoundException e) {
            Snackbar.make(getCurrentFocus(), getString(R.string.ErrorCreatingFile), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getString(Dismiss), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    }).show();
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showCleanHistoryAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.CleanHistoryConfirm));
        builder.setCancelable(true)
                .setPositiveButton(getString(R.string.Clean),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Topup.deleteAll(Topup.class);
                                updateTopUpList(getAllTopups());
                                setUnverifiedActions(HistoryActivity.this, 0);
                                setRibbonVisibility();
                            }
                        }).setNegativeButton(getString(R.string.Cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //hacer algo en el cancelar?
                    }
                });

        AlertDialog alert = builder.create();
        alert.setTitle(getString(R.string.CleanHistory));
        alert.show();
    }


    public Context get_this_context() {
        return this;
    }


    /*
    * Tarea para recibir y parsear los correos de respuesta del servicio
    * */
    private class NautaReader extends AsyncTask {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progessDialog = ProgressDialog.show(get_this_context(), "", getString(R.string.Receiving), true);
            cleanNewStatus();
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            progessDialog.hide();
            setRibbonVisibility();
            updateTopUpList(getAllTopups());
        }

        @Override
        protected Object doInBackground(Object[] params) {
            NautaMailReceiver receiver = new NautaMailReceiver(nautaUser, nautaPass);
            try {
                Map<String, List<String>> mailtexts = receiver.receiveMail("recharge");

                for (String subject : mailtexts.keySet()) {
                    for (String mailtext : mailtexts.get(subject)) {
                        String msg = Crypto.decodeBase64(mailtext);
                        if (subject.equals("recharge"))
                            processRechargeMail(getApplication(), msg);
                        else if (subject.equals("getStatus"))
                            processStatusMail(getApplication(), msg, false);
                        else if (subject.equals("cancel"))
                            processCancelMail(getApplication(), msg, false);
                        else if ("getBalance".equals(subject)) {
                            processBalance(getApplication(), msg);
                        } else if ("getReport".equals(subject)) {
                            processReport(getApplication(), msg);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Snackbar.make(getCurrentFocus(), getString(R.string.ConnectionError), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(Dismiss), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        }).show();
            }
            return true;
        }
    }

    /**
     * Procesa los correos entrantes con asusnto getBalance
     *
     * @param context
     * @param msg
     * @throws UnsupportedEncodingException
     */
    public static void processBalance(Context context, String msg) throws UnsupportedEncodingException {
        //ToDo: Manejar los objetos que no se pueden parsear
        ResponseBalance balace = JSONHelper.balanceFromJSON(msg);

        if (balace.getError() == null) {

            SharedPreferences preferences = context.getSharedPreferences("QvaTel_recargas", MODE_PRIVATE);

            long date = preferences.getLong("balance_date", 0);
            int abalance = 0;

            if (balace.getDate() > date) {
                date = balace.getDate();
                abalance = balace.getBalance();
                preferences.edit().putInt("balance", abalance).commit();
                preferences.edit().putLong("balance_date", date).commit();
            }
        }
    }

    /**
     * Procesa los correos entrantes con asusnto recharge
     *
     * @param context
     * @param msg
     * @throws UnsupportedEncodingException
     */
    public void processRechargeMail(Context context, String msg) throws UnsupportedEncodingException {
        processRechargeMail(context, msg, false);
    }

    /**
     * Procesa los correos entrantes con asusnto recharge
     *
     * @param context
     * @param msg
     * @throws UnsupportedEncodingException
     */
    public static void processRechargeMail(Context context, String msg, boolean toast) throws UnsupportedEncodingException {

        //ToDo: Manejar los objetos que no se pueden parsear
        ResponseTopUp tp = JSONHelper.topUpFromJSON(msg);
        if (tp.getUpdate()) {
            setUnverifiedActions(context, getUnverifiedActions(context) - 1);
        }
        if (tp.getError().equals("")) {
            for (Phone phone : tp.getPhones()) {
                //Aqui hay que cambia si el mae va a mandar el id mio local puedo entonces buscar por el.
                List<Topup> topups = Topup.find(Topup.class, "id = ?", String.valueOf(phone.getId()));
                //Las fechas que estoy recibiendo están en segundos y java trabaja con milisegundos
                //por eso la conversión forzosa a Long y la multiplicación por 1000
                if (topups.size() == 0) {
                    Topup topup = new Topup(phone.getNumber(), phone.getStatus(), String.valueOf(phone.getId()), new Date(Long.valueOf(phone.getDate()) * 1000), tp.getReference(), phone.getMonto());
                    topup.save();
                } else {
                    Topup topup = topups.get(0);
                    topup.setStatus(phone.getStatus());
                    topup.setDate(new Date(Long.valueOf(phone.getDate()) * 1000));
//                    topup.setReference(tp.getReference());
                    topup.save();
                }
            }
            if (tp.getPhones().length > 0) {
                if (toast) {
//                    try {
//                        Toast.makeText(context, context.getString(R.string.Updated) + " " + String.valueOf(tp.getPhones().length) + " " + context.getString(R.string.Recharges), Toast.LENGTH_LONG).show();
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
                } else
                    Snackbar.make(tvUnsend.getRootView(), context.getString(R.string.Updated) + " " + String.valueOf(tp.getPhones().length) + " " + context.getString(R.string.Recharges), Snackbar.LENGTH_INDEFINITE)
                            .setAction(context.getString(Dismiss), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            }).show();
            } else {
                if (toast) {
//                    Toast.makeText(context, context.getString(R.string.NoUpdates), Toast.LENGTH_LONG).show();
                } else
                    Snackbar.make(tvUnsend.getRootView(), context.getString(R.string.NoUpdates), Snackbar.LENGTH_INDEFINITE)
                            .setAction(context.getString(Dismiss), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            }).show();
            }
        } else {
            if (toast) {
//                Toast.makeText(context, tp.getError(), Toast.LENGTH_LONG).show();
            } else
                Snackbar.make(tvUnsend.getRootView(), tp.getError(), Snackbar.LENGTH_INDEFINITE)
                        .setAction(context.getString(Dismiss), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        }).show();
        }
    }

    /**
     * Procesa los correos entrantes con asusnto getStatus
     *
     * @param context
     * @param msg
     * @throws UnsupportedEncodingException
     */
    public static void processStatusMail(Context context, String msg, boolean toast) throws UnsupportedEncodingException {

        //ToDo: Manejar los objetos que no se pueden parsear
        ResponseStatus tp = JSONHelper.statusFromJSON(msg);

        if (tp.getStatus().equals("Desconocido")) {
            //Aqui de plano es eliminar el registro\
            Topup.deleteAll(Topup.class, "id = ?", new String[]{String.valueOf(tp.getId())});
        } else if (tp.getStatus().equals("Sin Procesar")) {
            //NO hago nda al menos por ahora
        } else {
            //Aqui tengo que actualizar
            List<Topup> topups = Topup.find(Topup.class, "id = ?", String.valueOf(tp.getId()));
            if (topups.size() == 0) {

            } else {
                Topup topup = topups.get(0);
                topup.setStatus(tp.getStatus());
                topup.setDate(new Date(Long.valueOf(tp.getDate()) * 1000));
                topup.save();
                if (toast) {
                } else
                    Snackbar.make(tvUnsend.getRootView(), context.getString(R.string.Updated1), Snackbar.LENGTH_INDEFINITE)
                            .setAction(context.getString(Dismiss), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            }).show();
            }
        }
    }

    /**
     * Procesa los correos entrantes con asusnto cancel
     *
     * @param context
     * @param msg
     * @throws UnsupportedEncodingException
     */
    public static void processCancelMail(Context context, String msg, boolean toast) throws UnsupportedEncodingException {

        //ToDo: Manejar los objetos que no se pueden parsear
        ResponseCancel tp = JSONHelper.cancelFromJSON(msg);

        if (tp.getSuccess() == 0) {
            //esto es que no se pudo eliminar.
            if (tp.getStatus().equals("Desconocido")) {
                //Aqui de plano es eliminar el registro
                Topup.deleteAll(Topup.class, "id = ?", new String[]{String.valueOf(tp.getId())});
            } else if (tp.getStatus().equals("Sin Procesar")) {
                //NO hago nda al menos por ahora
            } else {
                //Aqui tengo que actualizar
                List<Topup> topups = Topup.find(Topup.class, "id = ?", String.valueOf(tp.getId()));
                if (topups.size() == 0) {

                } else {
                    Topup topup = topups.get(0);
                    topup.setStatus(tp.getStatus());
                    topup.setDate(new Date(Long.valueOf(tp.getDate()) * 1000));
                    topup.save();
                    if (toast) {
                    } else
                        Snackbar.make(tvUnsend.getRootView(), context.getString(R.string.Updated1), Snackbar.LENGTH_INDEFINITE)
                                .setAction(context.getString(Dismiss), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                    }
                                }).show();
                }
            }
        } else {
            //aqui se elimino sin problemas
            Topup.deleteAll(Topup.class, "id = ?", new String[]{String.valueOf(tp.getId())});
        }
    }

    public static void processReport(Context context, String msg) throws UnsupportedEncodingException {
        Topup.deleteAll(Topup.class);
        ResponseTopUp phones = JSONHelper.topUpFromJSON(msg);

        if (phones.getPhones().length > 0) {
            for (Phone phone : phones.getPhones()) {
                //Aqui hay que cambia si el mae va a mandar el id mio local puedo entonces buscar por el.
                List<Topup> topups = Topup.find(Topup.class, "id = ?", String.valueOf(phone.getId()));
                //Las fechas que estoy recibiendo están en segundos y java trabaja con milisegundos
                //por eso la conversión forzosa a Long y la multiplicación por 1000
                if (topups.size() == 0) {
                    Topup topup = new Topup(phone.getNumber(), phone.getStatus(), String.valueOf(phone.getId()), new Date(Long.valueOf(phone.getDate()) * 1000), phone.getReference(), phone.getMonto());
                    topup.save();
                } else {
                    Topup topup = topups.get(0);

                    topup.setOpid((phone.getId() <= 0) ? "-2" : phone.getId() + "");
                    topup.setStatus(phone.getStatus());
                    topup.setDate(new Date(Long.valueOf(phone.getDate()) * 1000));
                    topup.setReference(phone.getReference());

                    if (phone.getId() > 0)
                        topup.setId(0L + phone.getId());
                    topup.save();
                }
            }
        }
    }

    public class StatusSenderTask extends AsyncTask<Void, Void, Integer> {

        private String TAG = "TagSender";
        private int errorcode = 1;
        public View view;

        public String command;
        public String body;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progessDialog = ProgressDialog.show(HistoryActivity.this, "", HistoryActivity.this.getString(R.string.Sending), true);
        }

        @Override
        protected void onPostExecute(Integer o) {
            super.onPostExecute(o);
            if (progessDialog != null)
                progessDialog.hide();
            if (HistoryActivity.this != null)
                if (view.getParent() != null)

                    Toast.makeText(HistoryActivity.this, (o == -1) ? HistoryActivity.this.getString(R.string.ConnectionError) : HistoryActivity.this.getString(R.string.SentOK), Toast.LENGTH_LONG).show();
//                    Snackbar.make(view, (o == -1) ? context.getString(R.string.ConnectionError) : context.getString(R.string.SentOK), Snackbar.LENGTH_INDEFINITE)
//                            .setAction(context.getString(R.string.Dismiss), new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//
//                                }
//                            }).show();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            NautaMailSender sender = new NautaMailSender(App.nautaUser, App.nautaPass);
            try {
                Log.i(TAG, "starting...");
//                int count = 0;
//                while (!DataNetwork.isNetworkConnected(App.getApplication().getApplication()) && count++ < 6) {
//                    try {
//                        Log.i(TAG, "waiting for network... ");
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//                if (!DataNetwork.isNetworkConnected(App.getApplication().getApplication())) {
//                    return -1;
//                }
                Log.i(TAG, "network OK. sending mail... to command" + command);
                sender.sendMail(command, Crypto.getBase64(body), Constants.SERVICE_MAIL);
                Log.i(TAG, "mail sent");
                errorcode = 0;
            } catch (Exception e) {
                e.printStackTrace();
                return -1;
            }
            return errorcode;
        }
    }

    public BroadcastReceiver force_update = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateTopUpList(getAllTopups());
        }
    };

    @Override
    protected void onResume() {

        registerReceiver(force_update, new IntentFilter(ImapPushService.FORCEPROCESSACTION));
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(force_update);
    }
}

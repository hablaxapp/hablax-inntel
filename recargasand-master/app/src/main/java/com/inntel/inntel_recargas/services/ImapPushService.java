package com.inntel.inntel_recargas.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.inntel.inntel_recargas.App;
import com.inntel.inntel_recargas.HistoryActivity;
import com.inntel.inntel_recargas.R;
import com.inntel.inntel_recargas.utils.Constants;
import com.inntel.inntel_recargas.utils.Crypto;
import com.inntel.inntel_recargas.utils.LifecycleHandler;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.protocol.FLAGS;
import com.sun.mail.imap.protocol.IMAPProtocol;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.FolderClosedException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.event.MessageCountAdapter;
import javax.mail.event.MessageCountEvent;
import javax.mail.internet.InternetAddress;


public class ImapPushService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_PUSH = "cu.qva2world.wxc.sender.wxcsms.Services.action.IMAPPUSH";

    public ImapPushService() {
        super("ImapPushService");
    }

    public static boolean running = false;

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startAction(Context context) {
        if (App.useImapush) {
            Log.i("ImapPush", "Iniciando el servicio");
            //Aqui tengo que verificar si el user quiere usar imap push
            Intent intent = new Intent(context, ImapPushService.class);
            intent.setAction(ACTION_PUSH);
            context.startService(intent);
        }
    }

    public static void stopAction(Context context) {
        Log.i("ImapPush", "Deteniendo el servicio");
        Intent intent = new Intent(context, ImapPushService.class);
        intent.setAction(ACTION_PUSH);
        context.stopService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_PUSH.equals(action)) {
                handleActionFoo();
            }
        }
    }

    private void handleActionFoo() {
        running = true;
        //Capturar los datos que se necesitan, por ahora cableare el imaphost y port
        String host = Constants.NAUTA_IMAP;
        int port = Constants.NAUTA_IMAP_PORT;
        String box = "INBOX";
        String user = App.nautaUser;
        if (!user.endsWith("@nauta.cu")) {
            user += "@nauta.cu";
        }
        String pass = App.nautaPass;
        //esta seria la frecuencia de consulta en caso de que no pueda activarse el idle

        int valueFreq = 5;//Integer.parseInt(Preferences.getsync_frequency());

        int freq = valueFreq * 60000;

        try {

            Properties props = System.getProperties();
            Log.i("ImapPush", "Getting session");
            // Get a Session object
            Session session = Session.getInstance(props, null);
            // session.setDebug(true);
            Log.i("ImapPush", "Session Getted");
            // Get a Store object
            Store store = session.getStore("imap");
            Log.i("ImapPush", "Connecting");
            // Connect
            store.connect(host, port, user, pass);

            // Open a Folder
            final Folder folder = store.getFolder(box);

            if (folder == null || !folder.exists()) {
                Log.i("ImapPush", "Invalid folder");
                return;
            }

            folder.open(Folder.READ_WRITE);
            Log.i("ImapPush", "Open folder success");
            // Add messageCountListener to listen for new messages
            folder.addMessageCountListener(new MessageCountAdapter() {
                public void messagesAdded(MessageCountEvent ev) {
                    Message[] msgs = ev.getMessages();
                    Log.i("ImapPush", "Got " + msgs.length + " new messages");

                    //Procesar elementos
                    // Just dump out the new messages
                    for (int i = 0; i < msgs.length; i++) {

                        try {
                            //si es de una de las direcciones que al menos ahora son las tipas
                            if (((InternetAddress) msgs[i].getFrom()[0]).getAddress().equals(Constants.RESPONSE_MAIL)) {

                                String subject = msgs[i].getSubject();

                                Log.i("ImapPush", "Message " +
                                        msgs[i].getMessageNumber() + ": " + subject);

//Aqui pongo todos los posibles asuntos para no analizar por gusto otros
                                if (msgs[i].getSubject().equals("recharge") ||
                                        msgs[i].getSubject().equals("getReport") ||
                                        msgs[i].getSubject().equals("getStatus") ||
                                        msgs[i].getSubject().equals("cancel") ||
                                        msgs[i].getSubject().equals("getBalance")) {
                                    String body = "";
                                    Part messagePart = msgs[i];
                                    Object content = messagePart.getContent();
                                    if (content instanceof Multipart) {
                                        messagePart = ((Multipart) content).getBodyPart(0);
                                    }
                                    String contentType = messagePart.getContentType();
                                    if (contentType.startsWith("text/plain")) {
                                        InputStream is = messagePart.getInputStream();
                                        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                                        String thisLine = reader.readLine();
                                        while (null != thisLine) {
                                            body += thisLine;
                                            thisLine = reader.readLine();
                                        }
                                    }

                                    //marco los mensajespara ser eliminados
                                    msgs[i].setFlag(FLAGS.Flag.DELETED, true);
//                                    msgs[i].setFlag(FLAGS.Flag.SEEN, true);

                                    //Ahora personalizo lo que me interesa solo
                                    String msg = Crypto.decodeBase64(body);
                                    if (msgs[i].getSubject().equals("recharge")) {
                                        //proceso los mensjes de info de recarga
                                        HistoryActivity.processRechargeMail(getApplication(), msg, true);
                                    } else if (subject.equals("getStatus"))
                                        HistoryActivity.processStatusMail(getApplication(), msg, false);
                                    else if (subject.equals("cancel"))
                                        HistoryActivity.processCancelMail(getApplication(), msg, false);
                                    else if ("getBalance".equals(subject)) {
                                        HistoryActivity.processBalance(getApplication(), msg);
                                    }
                                    else if ("getReport".equals(subject)) {
                                        HistoryActivity.processReport(getApplication(), msg);
                                    }


                                    if (!LifecycleHandler.isApplicationInForeground()) {
                                        //Aqui muestro una notificacion boba
                                        showNotification(getApplication());
                                    } else {
                                        Intent intent = new Intent();
                                        intent.setAction(FORCEPROCESSACTION);
                                        sendBroadcast(intent);
                                    }

                                    //Eliminar los correos que se encontraron y marcaron como eliminables.
                                    try {
                                        folder.expunge();
                                    } catch (MessagingException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }


                        } catch (IOException ioex) {
                            ioex.printStackTrace();
                        } catch (MessagingException mex) {
                            mex.printStackTrace();
                        }
                    }


                    //Aqui falta averiguar si hay algo por lo que notificar si no hay nada porque hacerlo no se hace nada


                }
            });

            // Check mail once in "freq" MILLIseconds

            boolean supportsIdle = false;
            try {
                if (folder instanceof IMAPFolder) {
                    IMAPFolder f = (IMAPFolder) folder;
                    f.idle();
                    supportsIdle = true;
                    //Esto es para mantener el idle activo
                    Thread t = new Thread(
                            new KeepAliveRunnable(f), "IdleConnectionKeepAlive"
                    );

                    t.start();
                }
            } catch (FolderClosedException fex) {
                throw fex;
            } catch (MessagingException mex) {
                supportsIdle = false;
            }
            for (; ; ) {
                if (supportsIdle && folder instanceof IMAPFolder) {
                    IMAPFolder f = (IMAPFolder) folder;
                    f.idle();
                    System.out.println("IDLE done");
                } else {
                    Thread.sleep(freq); // sleep for freq milliseconds

                    // This is to force the IMAP server to send us
                    // EXISTS notifications.
                    folder.getMessageCount();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.i("ImapPush", e.getMessage());
            stopSelf();
        }
    }

    public static final String FORCEPROCESSACTION = "cu.qva2world.qvatel.action.datachange.force.update";

    public void showNotification(Context context) {

        try {
//            Intent intent = new Intent(context, RechargeActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


            NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.noborder)
                    .setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(R.mipmap.noborder)).getBitmap()) //Aqui poner un icono de cake o al go asi o construir uno a partir de un cake y la foto del contacto si tiene
                    .setContentTitle("Se ha actualizado la lista de elementos del historia.")
                    .setContentText("Presione para abrir.")
//                .setOngoing(true)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
//                        .setLights(235, 10,10)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))//aqui buscar un sonido para que al salir la notificacion haga bit o algo o como sacar la de por defecto del sistema
//                    .setTicker("Recibido " + monto + " CUC de " + name)
//                    .setContentIntent(PendingIntent.getActivity(context, 15400, intent, PendingIntent.FLAG_CANCEL_CURRENT))
                    .setVibrate(new long[]{1000L, 1000L/*, 1000L, 1000L, 1000L*/});
            ((android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify(1363, builder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Aqui podria perfectamente cerrar lo qe pueda ser cerrado
        running = false;
    }

    private static class KeepAliveRunnable implements Runnable {

        private static final long KEEP_ALIVE_FREQ = 300000; // 5 minutes

        private IMAPFolder folder;

        public KeepAliveRunnable(IMAPFolder folder) {
            this.folder = folder;
        }

        @Override
        public void run() {
            while (!Thread.interrupted()) {
                try {
                    Thread.sleep(KEEP_ALIVE_FREQ);

                    // Perform a NOOP just to keep alive the connection
                    Log.d("ImapPush", "Performing a NOOP to keep alvie the connection");
                    folder.doCommand(new IMAPFolder.ProtocolCommand() {
                        public Object doCommand(IMAPProtocol p)
                                throws ProtocolException {
                            p.simpleCommand("NOOP", null);
                            return null;
                        }
                    });
                } catch (InterruptedException e) {
                    // Ignore, just aborting the thread...
                } catch (MessagingException e) {
                    // Shouldn't really happen...
                    Log.w("ImapPush", "Unexpected exception while keeping alive the IDLE connection", e);
                }
            }
        }
    }

}

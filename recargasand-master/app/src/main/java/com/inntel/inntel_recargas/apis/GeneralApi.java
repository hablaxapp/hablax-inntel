package com.inntel.inntel_recargas.apis;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by apple on 3/1/16.
 */
public abstract class GeneralApi {

    protected static final Integer REQUEST_CODE_SUCCESS = 1;
    protected static final Integer REQUEST_CODE_FAILED = 0;
    protected static final Integer REQUEST_CODE_AUTHORIZE = 2;

    protected static void unwrapApiResponseCallback(ApiListener apiListener, String response) {
        ApiResponse resultResponse = new ApiResponse();
        try {
            JSONObject jsonResponse = new JSONObject(response);
            resultResponse.statusCode = Integer.parseInt(jsonResponse.getString("Status"));
            resultResponse.message = jsonResponse.getString("Message");
            resultResponse.data = jsonResponse;
            if (resultResponse.statusCode == REQUEST_CODE_SUCCESS) {
                resultResponse.resultSuccess = true;
            } else {
                resultResponse.resultSuccess = false;
            }

        } catch (JSONException e) {
            resultResponse.resultSuccess = false;
        }

        if (resultResponse.resultSuccess) {
            apiListener.success(resultResponse);
        } else {
            apiListener.failure(resultResponse.message);
        }
    }

    public static class ApiResponse {
        public int statusCode;
        public String message;
        public boolean resultSuccess;
        public JSONObject data;
        public String dataAsString;
    }

    public interface ApiListener {
        public void success(ApiResponse response);
        public void failure(String error);
    }

}

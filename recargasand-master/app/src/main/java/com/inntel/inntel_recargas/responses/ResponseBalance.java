package com.inntel.inntel_recargas.responses;

/**
 * Created by akiel on 6/3/17.
 */

public class ResponseBalance {
    String action;
    Integer balance;
    String error;
    long date;

    public ResponseBalance(String action, Integer balance, String error, long date) {
        this.action = action;
        this.balance = balance;
        this.error = error;
        this.date = date;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }
}

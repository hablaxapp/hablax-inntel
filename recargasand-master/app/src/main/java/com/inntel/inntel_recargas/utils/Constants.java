package com.inntel.inntel_recargas.utils;

/**
 * Created by akiel on 5/28/17.
 */

public class Constants {
    //correo al que se envían y del que se reciben
    public static final String SERVICE_MAIL = "recharges.inntel@gmail.com";
    public static final String RESPONSE_MAIL = "recharges.inntel@gmail.com";

    //configuración para nauta
    public static final String NAUTA_IMAP = "imap.nauta.cu";//"181.225.231.14";
    public static final Integer NAUTA_IMAP_PORT = 143;

    public static final String NAUTA_SMTP = "smtp.nauta.cu";//"181.225.231.12";
    public static final Integer NAUTA_SMTP_PORT = 25;


    public static final Integer RECEIVERS_DELAY = 20;
}

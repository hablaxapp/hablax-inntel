package com.inntel.inntel_recargas.apis;

import java.io.IOException;

/**
 * Created by apple on 3/2/16.
 */
public class ServiceApi extends GeneralApi {

    public class Constants {
        public static final String API_HOSTNAME = "www.innoverit.com";

    }

    private static final String WEBSERVICE_GET_SEND_RECHARGES = "https://"+ Constants.API_HOSTNAME+"/api_recharges/recharge_from_app?product=%s&token=%s&operator_id=161&number=%s"; // CubaCel -> 161 / Etecsa Nauta -> 2174


    public static ApiResponse apiGetSendRecharges(ApiListener apiListener, String product, String apikey, String number) {
        ApiResponse apiResponse = new ApiResponse();
        try {
            String response = ApiUtility.api(ApiUtility.HTTP_GET, null, null, WEBSERVICE_GET_SEND_RECHARGES, product, apikey, number);
            apiResponse.resultSuccess = true;
            apiResponse.message = response;
            apiListener.success(apiResponse);
            return apiResponse;

        } catch (IOException e) {
            apiResponse.resultSuccess = false;
            apiListener.failure(e.getMessage());
            return apiResponse;
        }
    }


}

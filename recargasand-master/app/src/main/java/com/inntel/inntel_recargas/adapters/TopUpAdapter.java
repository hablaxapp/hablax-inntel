package com.inntel.inntel_recargas.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.inntel.inntel_recargas.App;
import com.inntel.inntel_recargas.R;
import com.inntel.inntel_recargas.mail.NautaMailSender;
import com.inntel.inntel_recargas.models.Topup;
import com.inntel.inntel_recargas.recivers.NetworkChangeReceiver;
import com.inntel.inntel_recargas.utils.ConnectionHelper;
import com.inntel.inntel_recargas.utils.Constants;
import com.inntel.inntel_recargas.utils.Crypto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by akiel on 5/26/17.
 */

public class TopUpAdapter extends ArrayAdapter<Topup> {

    private final Context context;
    private final Topup[] values;
    private int mLastPosition;
    private ProgressDialog progessDialog;

    public TopUpAdapter(Context context, Topup[] values) {
        super(context, R.layout.topup_item_layout, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.topup_item_layout, parent, false);
        TextView number = (TextView) rowView.findViewById(R.id.textViewNumber);
        final TextView status = (TextView) rowView.findViewById(R.id.textViewStatus);
        TextView date = (TextView) rowView.findViewById(R.id.textViewDate);
        final TextView reference = (TextView) rowView.findViewById(R.id.textViewReference);
        CardView isNew = (CardView) rowView.findViewById(R.id.cardViewNew);

        if (position % 2 == 1) {
            rowView.setBackgroundColor(Color.parseColor("#EEEEEE"));
        } else {
            rowView.setBackgroundColor(Color.WHITE);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        date.setText(sdf.format(values[position].getDate()));

        String referenceValue = "";

        if (values[position].getReference() != null && !values[position].getReference().equals("")) {
            referenceValue += values[position].getReference();
//            reference.setText(values[position].getReference());
        }

        if(values[position].getMonto() != 0){
            referenceValue += "  ($"+ values[position].getMonto() + ") ";
        }
        reference.setText(referenceValue);
        if(referenceValue.equals("")) {
            reference.setVisibility(View.GONE);
        }

        if (values[position].getNew()) {
            isNew.setVisibility(View.VISIBLE);
        } else {
            isNew.setVisibility(View.INVISIBLE);
        }

        number.setText(values[position].getPhone().substring(3));
        if (values[position].getStatus().equals("Recargado")) {
            status.setText("{fa-check-circle}");
            status.setTextColor(Color.parseColor("#5cb85c"));
        } else if (values[position].getStatus().equals("Fallida")) {
            status.setText("{fa-minus-circle}");
            status.setTextColor(Color.parseColor("#d9534f"));
        } else if (values[position].getStatus().equals("Pendiente")) {
            status.setText("{fa-circle-thin}");
            status.setTextColor(context.getResources().getColor(R.color.colorGrisPendiente));// Color.parseColor("#949292"));
        } else if (values[position].getStatus().equals("Sin Verificar")) {
            status.setText("{fa-send}");
            status.setTextColor(context.getResources().getColor(R.color.colorGrisPendiente));// Color.parseColor("#949292"));
        } else {
            status.setText("{fa-clock-o}");
            status.setTextColor(Color.parseColor("#f0ad4e"));
        }

        status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TopUpAdapter.this.values[position].getOpid().equals("-2"))
                    Toast.makeText(TopUpAdapter.this.context, "Recarga desde la web", Toast.LENGTH_LONG).show();
                else {

                    if (values[position].getStatus().equals("Recargado")) {
                        Toast.makeText(context, values[position].getStatus(), Toast.LENGTH_LONG).show();
                    } else if (values[position].getStatus().equals("Fallida")) {
                        Toast.makeText(context, values[position].getStatus(), Toast.LENGTH_LONG).show();
//                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                        builder.setMessage(context.getString(R.string.SelectOperation));
//                        builder.setCancelable(true)
//                                .setPositiveButton("Reintentar",
//                                        new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                try {
//                                                    //Prepare body
//                                                    List<JSONHelper.tempdata> data = new ArrayList<JSONHelper.tempdata>();
//
//                                                    Topup topup = new Topup(values[position].getPhone(), "Sin Verificar", "-1", new Date(), "");
//                                                    long ids = topup.save();
//                                                    data.add(new JSONHelper.tempdata(Integer.parseInt(ids + ""), values[position].getPhone()));
//                                                    Date date = new Date();
//                                                    Map<String, Object> requestParams = new TreeMap<String, Object>();
//                                                    requestParams.put("action", "recharge");
//                                                    requestParams.put("token", App.inntelToken);
//                                                    requestParams.put("reference", values[position].getReference());
//                                                    requestParams.put("date", date.getTime());
//                                                    requestParams.put("phones", data);
//                                                    requestParams.put("code", Crypto.getSHA1(App.inntelToken + String.valueOf(date.getTime())));
//
//                                                    sendData(status, "recharge", requestParams);
//
//
//                                                } catch (Exception e) {
//
//                                                }
//                                            }
//                                        })
////                            .setNegativeButton(context.getString(R.string.Cancel),
////                                    new DialogInterface.OnClickListener() {
////                                        public void onClick(DialogInterface dialog, int id) {
//////
////                                        }
////                                    })
//                        ;
//
//                        AlertDialog alert = builder.create();
//                        alert.setTitle("Comandos");
//                        alert.show();

                    } else if (values[position].getStatus().equals("Pendiente")) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(context.getString(R.string.SelectOperation));
                        builder.setCancelable(true)
                                .setPositiveButton(context.getString(R.string.update),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                try {
                                                    //Prepare body
                                                    Date date = new Date();
                                                    Map<String, Object> requestParams = new TreeMap<String, Object>();
                                                    requestParams.put("action", "getStatus");
                                                    requestParams.put("token", App.inntelToken);
                                                    requestParams.put("id", values[position].getId());
                                                    requestParams.put("date", date.getTime());
                                                    requestParams.put("code", Crypto.getSHA1(App.inntelToken + String.valueOf(date.getTime())));

                                                    sendData(status, "getStatus", requestParams);

                                                } catch (Exception e) {

                                                }
                                            }
                                        })
//                            .setNegativeButton(context.getString(R.string.Cancel),
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
////
//                                        }
//                                    })
                        ;

                        AlertDialog alert = builder.create();
                        alert.setTitle("Comandos");
                        alert.show();


                    } else if (values[position].getStatus().equals("Programada")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(context.getString(R.string.SelectOperation));
                        builder.setCancelable(true)
                                .setPositiveButton(context.getString(R.string.update),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                try {
                                                    //Prepare body
                                                    Date date = new Date();
                                                    Map<String, Object> requestParams = new TreeMap<String, Object>();
                                                    requestParams.put("action", "getstatus");
                                                    requestParams.put("token", App.inntelToken);
                                                    requestParams.put("id", values[position].getId());
                                                    requestParams.put("date", date.getTime());
                                                    requestParams.put("code", Crypto.getSHA1(App.inntelToken + String.valueOf(date.getTime())));
                                                    sendData(status, "getStatus", requestParams);
                                                } catch (Exception e) {

                                                }
                                            }
                                        })
                                .setNegativeButton(context.getString(R.string.Cancel),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                try {
                                                    //Prepare body
                                                    Date date = new Date();
                                                    Map<String, Object> requestParams = new TreeMap<String, Object>();
                                                    requestParams.put("action", "cancel");
                                                    requestParams.put("token", App.inntelToken);
                                                    requestParams.put("id", values[position].getId());
                                                    requestParams.put("date", date.getTime());
                                                    requestParams.put("code", Crypto.getSHA1(App.inntelToken + String.valueOf(date.getTime())));
                                                    sendData(status, "cancel", requestParams);
                                                } catch (Exception e) {

                                                }
                                            }
                                        });

                        AlertDialog alert = builder.create();
                        alert.setTitle("Comandos");
                        alert.show();
                    } else {

                    }
                }


            }
        });

        return rowView;
    }

    Gson g = new Gson();

    private void sendData(final View view, final String command, final Map<String, Object> requestParams) {

        ConnectionHelper.connect(context, new ConnectionHelper.ConnectListener() {
            @Override
            public void connectedOK(NetworkChangeReceiver.ConnectStatus status) {
                StatusSenderTask getStatus = new StatusSenderTask();
                getStatus.view = view;
                getStatus.command = "recharge";
                getStatus.body = g.toJson(requestParams);
                getStatus.execute();
            }

            @Override
            public void connectedBLocked() {
//                        Toast.makeText(MainActivity.this, "Debe estar conectado para poder enviar o recibir.", Toast.LENGTH_LONG).show();
            }

            @Override
            public void connectedFail(Exception e, String error) {
                Snackbar.make(view, context.getString(R.string.InactiveDataRecv), Snackbar.LENGTH_INDEFINITE)
                        .setAction(context.getString(R.string.Dismiss), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        }).show();
            }
        });


    }


    public class StatusSenderTask extends AsyncTask<Void, Void, Integer> {

        private String TAG = "TagSender";
        private int errorcode = 1;
        public View view;

        public String command;
        public String body;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progessDialog = ProgressDialog.show(context, "", context.getString(R.string.Sending), true);
        }

        @Override
        protected void onPostExecute(Integer o) {
            super.onPostExecute(o);
            if (progessDialog != null)
                progessDialog.hide();
            if (context != null)
                if (view.getParent() != null)

                    Toast.makeText(context, (o == -1) ? context.getString(R.string.ConnectionError) : context.getString(R.string.SentOK) , Toast.LENGTH_LONG ).show();
//                    Snackbar.make(view, (o == -1) ? context.getString(R.string.ConnectionError) : context.getString(R.string.SentOK), Snackbar.LENGTH_INDEFINITE)
//                            .setAction(context.getString(R.string.Dismiss), new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//
//                                }
//                            }).show();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            NautaMailSender sender = new NautaMailSender(App.nautaUser, App.nautaPass);
            try {
                Log.i(TAG, "starting...");
//                int count = 0;
//                while (!DataNetwork.isNetworkConnected(App.getApplication()) && count++ < 6) {
//                    try {
//                        Log.i(TAG, "waiting for network... ");
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//                if (!DataNetwork.isNetworkConnected(App.getApplication())) {
//                    return -1;
//                }
                Log.i(TAG, "network OK. sending mail... to command" + command);
                sender.sendMail(command, Crypto.getBase64(body), Constants.SERVICE_MAIL);
                Log.i(TAG, "mail sent");
                errorcode = 0;
            } catch (Exception e) {
                e.printStackTrace();
                return -1;
            }
            return errorcode;
        }
    }
}

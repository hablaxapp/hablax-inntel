package com.inntel.inntel_recargas.utils;

import android.content.Context;

import com.inntel.inntel_recargas.adapters.NumberAdapter;
import com.inntel.inntel_recargas.models.Topup;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by akiel on 5/23/17.
 */

public final class RegularExpressions {

    public static final String pattern = "(5\\d{7})\\D*";

    /*
    * Mina los números de teléfono en una cadena
    * */
    public static ArrayList<NumberAdapter.PhoneRequestContainer> getPhoneNumbers(Context context, String string) {
        //test
        string = string.replace("+53", "");
        //test end
        ArrayList<NumberAdapter.PhoneRequestContainer> result = new ArrayList<>();
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(string);
        while (m.find()) {
            String number = m.group(1).toString();

            //Aqui debo proceder para ver si hay algun elemento en el historial que tenga este numero para si es asi
            //activar el mostrar info
           long count  = Topup.count(Topup.class, "phone = ?", new String[]{"+53" + number});
//
//            List<Topup> list = Topup.find(Topup.class, "", new String[]{});

            result.add(new NumberAdapter.PhoneRequestContainer(context, number, count > 0));
        }
        return result;
    }

    public static boolean isPhoneNumber(String string){
        return string.matches(pattern);
    }
}



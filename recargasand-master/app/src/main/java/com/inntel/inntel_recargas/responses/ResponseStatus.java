package com.inntel.inntel_recargas.responses;

/**
 * Created by chenry on 4/8/17.
 */

public class ResponseStatus {
    private String action;
    private String status;
    private long date;
    private long id;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

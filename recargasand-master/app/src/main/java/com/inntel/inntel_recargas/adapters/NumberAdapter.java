package com.inntel.inntel_recargas.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.joanzapata.iconify.widget.IconTextView;
import com.inntel.inntel_recargas.R;
import com.inntel.inntel_recargas.fragments.MontoPickerDialogFragment;
import com.inntel.inntel_recargas.fragments.NumberPickerDialogFragment;
import com.inntel.inntel_recargas.models.Topup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by akiel on 5/26/17.
 */

public class NumberAdapter extends ArrayAdapter<NumberAdapter.PhoneRequestContainer> {

    private final Activity context;
    private final ArrayList<PhoneRequestContainer> values;

    public NumberAdapter(Activity context, ArrayList<PhoneRequestContainer> values) {
        super(context, R.layout.topup_item_layout, values);
        this.context = context;
        this.values = values;
    }

    public ArrayList<PhoneRequestContainer> getList() {
        return values;
    }

    public NumberAdapter getAdapter() {
        return this;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.number_item_layout, parent, false);
        TextView number = (TextView) rowView.findViewById(R.id.textViewNumberItem);
        number.setText(values.get(position).getNumber());

        if (position % 2 == 1) {
            rowView.setBackgroundColor(Color.parseColor("#EEEEEE"));
        } else {
            rowView.setBackgroundColor(Color.WHITE);
        }

        IconTextView delete = (IconTextView) rowView.findViewById(R.id.deleteNumber);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(context.getString(R.string.CleanInfoConfirm));
                builder.setCancelable(true)
                        .setPositiveButton(context.getString(R.string.Clean),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        values.remove(position);
                                        getAdapter().notifyDataSetChanged();
                                    }
                                }).setNegativeButton(context.getString(R.string.Cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //nothing on cancel
                            }
                        });

                AlertDialog alert = builder.create();
                alert.setTitle(context.getString(R.string.Clean));
                alert.show();

            }
        });

        final TextView cantView = (TextView) rowView.findViewById(R.id.cantNumber);
        cantView.setText(values.get(position).getCant() + "");
        cantView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                NumberPickerDialogFragment fragment = new NumberPickerDialogFragment();
                fragment.listener = new NumberPickerDialogFragment.OnPickListener() {
                    @Override
                    public void pick(int value) {
                        cantView.setText(value + "");
                        values.get(position).setCant(value);
                    }
                };
                fragment.defaultvalue = Integer.parseInt(cantView.getText().toString());
                fragment.show(context.getFragmentManager(), null);
            }
        });
        final TextView info = (TextView) rowView.findViewById(R.id.info);
        if (values.get(position).isMore()) {


//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//                info.setBackground(context.getResources().getDrawable(R.drawable.info_on));
//            }else{
            info.setBackgroundResource(R.drawable.info_on);
//            }
            //Cambio el color al activo
            info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {

                    List<Topup> list = Topup.find(Topup.class, "phone = ? and status != ? and status != ?", new String[]{"+53" + values.get(position).getNumber(), "Sin Verificar", "Fallida"});
                    //Aui tengo que mostrar el listado de los elementos que resultaron de la busqieda
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                    dialog.setDoubleLineItems()
                    String[] result = new String[list.size()];
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
                    int count = 0;
                    for (Topup t : list) {
//                        dialog.addMenuItem(sdf.format(t.getDate()), count++);
                        result[count++] = sdf.format(t.getDate());
                    }
                    builder.setItems(result, null);
                    builder.setTitle("Repeticiones");
                    builder.setPositiveButton("Cerrar", null);
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();

                }

            });
        } else {
//            reseteo el colo off
//            info.setBackground(context.getResources().getDrawable(R.drawable.info_on));
            info.setBackgroundResource(R.drawable.info_off);
        }
        final TextView comentary = (TextView) rowView.findViewById(R.id.comentary);
        if (values.get(position).getReference() != null && !values.get(position).getReference().equals("")) {
            if (values.get(position).isGeneral())
                comentary.setBackgroundResource(R.drawable.info_on_general);
            else
                comentary.setBackgroundResource(R.drawable.info_on);
        } else {
            comentary.setBackgroundResource(R.drawable.info_off);
        }
        comentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Asigna Comentario");
                builder.setNegativeButton("Cancelar", null);
                View view = View.inflate(context, R.layout.comentary_layout, null);
                final EditText text = (EditText) view.findViewById(R.id.text);
                if (values.get(position).getReference() != null)
                    text.setText(values.get(position).getReference());
                builder.setView(view);
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (text.getText().toString().equals("")) {
                            Toast.makeText(context, "Debe especificar un comentario.", Toast.LENGTH_LONG).show();
                            return;
                        }
                        values.get(position).setReference(text.getText().toString());
                        comentary.setBackgroundResource(R.drawable.info_on);
                        dialogInterface.dismiss();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }

        });

        final TextView montoView = (TextView) rowView.findViewById(R.id.cantMonto);
        //Aqui se carga el valor por defecto

//        if (montoView.getText().toString().length() == 0) {
//            SharedPreferences pref = context.getSharedPreferences("QvaTel_recargas", context.MODE_PRIVATE);
//            switch (pref.getInt("monto_defecto", 1)) {
//                case 0:
//                    montoView.setText("15");
//                    break;
//                case 1:
//                    montoView.setText("20");
//                    break;
//                case 2:
//                    montoView.setText("35");
//                    break;
//            }
//        }
        montoView.setText(values.get(position).getMonto() + "");
        montoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                MontoPickerDialogFragment fragment = new MontoPickerDialogFragment();
                fragment.listener = new MontoPickerDialogFragment.OnMontoPickListener() {
                    @Override
                    public void pick(int value) {
                        switch (value) {
                            case 0:
                                montoView.setText("15");
                                values.get(position).setMonto(15);
                                break;
                            case 1:
                                montoView.setText("20");
                                values.get(position).setMonto(20);
                                break;
                            case 2:
                                montoView.setText("25");
                                values.get(position).setMonto(25);
                                break;
                        }
                    }
                };
                switch (Integer.parseInt(montoView.getText().toString())) {
                    case 15:
                        fragment.defaultvalue = 0;
                        break;
                    case 20:
                        fragment.defaultvalue = 1;
                        break;
                    case 25:
                        fragment.defaultvalue = 2;
                        break;
                }

                fragment.show(context.getFragmentManager(), null);
            }
        });


        return rowView;
    }

    public static class PhoneRequestContainer {
        private String number;
        private int cant;
        private int monto = 20;
        private boolean isGeneral;
        private boolean more = false;
        private String reference = null;

        public PhoneRequestContainer(Context context, String number) {
            this.number = number;
            this.cant = 1;

            SharedPreferences pref = context.getSharedPreferences("QvaTel_recargas", context.MODE_PRIVATE);
            switch (pref.getInt("monto_defecto", 1)) {
                case 0:
                    monto = (15);
                    break;
                case 1:
                    monto = (20);
                    break;
                case 2:
                    monto = (35);
                    break;
            }
        }

        public PhoneRequestContainer(Context context, String number, boolean more) {
            this.number = number;
            this.cant = 1;
            this.more = more;
            SharedPreferences pref = context.getSharedPreferences("QvaTel_recargas", context.MODE_PRIVATE);
            switch (pref.getInt("monto_defecto", 1)) {
                case 0:
                    monto = (15);
                    break;
                case 1:
                    monto = (20);
                    break;
                case 2:
                    monto = (35);
                    break;
            }
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public int getCant() {
            return cant;
        }

        public void setCant(int cant) {
            this.cant = cant;
        }

        public boolean isMore() {
            return more;
        }

        public void setMore(boolean more) {
            this.more = more;
        }

        public int getMonto() {
            return monto;
        }

        public void setMonto(int monto) {
            this.monto = monto;
        }

        public String getReference() {
            return reference;
        }

        public void setReference(String reference) {
            this.reference = reference;
        }

        public boolean isGeneral() {
            return isGeneral;
        }

        public void setGeneral(boolean general) {
            isGeneral = general;
        }
    }

}

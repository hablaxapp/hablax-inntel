package com.inntel.inntel_recargas.models;

import com.orm.SugarRecord;

import java.util.Date;

/**
 * Created by akiel on 5/24/17.
 */

public class Topup extends SugarRecord {
    /**
     * Es el numero recado
     */
    String phone;
    /**
     * Este puede tomar valores Recargado, Fallida y "Programado" y este indigca el estado de esta recarga
     */
    String status;

    public Boolean getNew() {
        return isnew;
    }

    public void setNew(Boolean aNew) {
        isnew = aNew;
    }

    /**
     * Es el id de unico del server
     */
    String opid;
    /**
     * Es o una cadena o vacio en caso de cadena es un agrupador de recarga en lotes.
     */
    String reference;
    /**
     * Fecha del server en segundos por lo que tengo que tener en cuenta ya que java es en milisegundos
     */
    Date date;
    /**
     * es usado para marcar visualmente las tuplas que se actualizaron al final
     */
    Boolean isnew;

    private int monto;

    public Topup() {
    }

    public Topup(String phone, String status, String opid, Date date, String reference, int monto) {
        this.phone = phone;
        this.status = status;
        this.opid = opid;
        this.date = date;
        this.reference = reference;
        this.isnew = true;
        this.setMonto(monto);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOpid() {
        return opid;
    }

    public void setOpid(String opid) {
        this.opid = opid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public int getMonto() {
        return monto;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }
}

package com.inntel.inntel_recargas.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.NumberPicker;

/**
 * Created by chenry on 2/8/17.
 */

public class NumberPickerDialogFragment extends DialogFragment {

    public OnPickListener listener = null;
    public int defaultvalue = 1;

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//
//
//        NumberPicker num = new NumberPicker(getActivity());
//
//        num.setMaxValue(15);
//        num.setMinValue(1);
//        num.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
//            @Override
//            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
//                if (listener != null)
//                    listener.pick(i1);
//            }
//        });
//
//
//        return num;
//    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        NumberPicker num = new NumberPicker(getActivity());


        num.setMaxValue(15);
        num.setMinValue(1);
        num.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                if (listener != null)
                    listener.pick(i1);
            }
        });
        num.setValue(defaultvalue);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Escoge cantidad");
        builder.setView(num);

        builder.setPositiveButton("Cerrar", null);

        return builder.create();//super.onCreateDialog(savedInstanceState);
    }

    public interface OnPickListener {
        void pick(int value);
    }
}

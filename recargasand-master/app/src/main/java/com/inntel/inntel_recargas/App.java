package com.inntel.inntel_recargas;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.Build;

import com.inntel.inntel_recargas.services.ImapPushService;
import com.inntel.inntel_recargas.utils.LifecycleHandler;


/**
 * Created by chenry on 13/2/17.
 */
public class App extends Application {

    private String mCountryIso;
    private static App app = null;
    public static String nautaUser;
    public static String nautaPass;
    public static boolean useImapush;
    public static String inntelToken;
    public static String method;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            registerActivityLifecycleCallbacks(new LifecycleHandler());
        }
        loadCredentials();
        if (useImapush)
            ImapPushService.startAction(this);

//        //inicializa la DDBB y la crea si no existe
//        SugarContext.init(getApplication());
//        SchemaGenerator schemaGenerator = new SchemaGenerator(this);
//        schemaGenerator.createDatabase(new SugarDb(this).getDB());
    }

    public void loadCredentials() {
        SharedPreferences preferences = getSharedPreferences("QvaTel_recargas", MODE_PRIVATE);
        nautaUser = preferences.getString("user", "");
        nautaPass = preferences.getString("pass", "");
        inntelToken = preferences.getString("token", "");
        useImapush = preferences.getBoolean("imapPush", true);


    }

    synchronized public static App getApplication() {
        return app;
    }

}

package com.inntel.inntel_recargas.mail;

import android.util.Log;

import com.inntel.inntel_recargas.utils.Constants;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import javax.mail.Authenticator;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FromStringTerm;

/**
 * Created by akiel on 5/28/17.
 */

public class NautaMailReceiver extends Authenticator {

    public static final String TAG = "NautaMailReceiver";

    private String username;
    private String password;
    private Session session;

    public NautaMailReceiver(String username, String password) {
        this.username = username;
        this.password = password;
        if (!this.username.endsWith("@nauta.cu")) {
            this.username += "@nauta.cu";
        }
        Properties properties = System.getProperties();
        session = Session.getDefaultInstance(properties, this);
    }

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password);
    }

    public synchronized Map<String, List<String>> receiveMail(final String subject) throws Exception {
        Store store = null;
        Folder folder = null;
        Map<String, List<String>> result = new TreeMap<>();

        try {
            Log.i(TAG, "Conectando " + Constants.NAUTA_IMAP + ":" + Constants.NAUTA_IMAP_PORT);
            store = session.getStore("imap");
            store.connect(Constants.NAUTA_IMAP, Constants.NAUTA_IMAP_PORT, username, password);

            Log.i(TAG, "Obteniendo INBOX");
            folder = store.getFolder("INBOX");
            if (null == folder) {
                throw new Exception("Can't get INBOX");
            }

            Log.i(TAG, "buscando correos de " + Constants.RESPONSE_MAIL);
            folder.open(Folder.READ_WRITE);
//            Message[] messages = folder.search(new SearchTerm() {
//                @Override
//                public boolean match(Message msg) {
//                    try {
//                        return msg.getFrom()[0].toString().contains(Constants.RESPONSE_MAIL) ;//&& msg.getSubject().contains(subject);
//                    } catch (MessagingException e) {
//                        Log.e(TAG, e.getLocalizedMessage());
//                        return false;
//                    }
//                }
//            });

            FromStringTerm fromTerm = new FromStringTerm(Constants.RESPONSE_MAIL);
            Message[] messages = folder.search(fromTerm);

            Log.i(TAG, "Count: " + messages.length);

            for (Message message : messages) {
                String subjects = message.getSubject();
//                if (subject.equals("getBalance") && !subjects.equals("getBalance")) continue;//esto evita que se processen otros correos al procesar el balance
//                if (!message.isSet(Flags.Flag.SEEN)) {
                String text = "";
                Part messagePart = message;
                Object content = messagePart.getContent();
                if (content instanceof Multipart) {
                    messagePart = ((Multipart) content).getBodyPart(0);
                }
                String contentType = messagePart.getContentType();
                if (contentType.startsWith("text/plain")) {
                    InputStream is = messagePart.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    String thisLine = reader.readLine();
                    while (null != thisLine) {
                        text += thisLine;
                        thisLine = reader.readLine();
                    }
                }
                if (result.containsKey(subjects)) {
                    result.get(subjects).add(text);
                } else {
                    result.put(subjects, new ArrayList<String>());
                    result.get(subjects).add(text);
                }
                message.setFlag(Flags.Flag.SEEN, true);
                message.setFlag(Flags.Flag.DELETED, true);
//                }

            }
            folder.expunge();

        } finally {
            Log.i(TAG, "Cerrando");
            try {
                if (null != folder) {
                    folder.close(true);
                }
                if (null != store) {
                    store.close();
                }
            } catch (MessagingException me) {
                Log.e(TAG, me.getLocalizedMessage());
            }
        }
        Log.i(TAG, "Finalizado");
        return result;
    }
}

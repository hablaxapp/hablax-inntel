package com.inntel.inntel_recargas.utils;

import com.google.gson.Gson;
import com.inntel.inntel_recargas.adapters.NumberAdapter;
import com.inntel.inntel_recargas.models.Topup;
import com.inntel.inntel_recargas.responses.ResponseBalance;
import com.inntel.inntel_recargas.responses.ResponseCancel;
import com.inntel.inntel_recargas.responses.ResponseStatus;
import com.inntel.inntel_recargas.responses.ResponseTopUp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by akiel on 5/28/17.
 */

public final class JSONHelper {


    public static class tempdata {
        private int id;
        private String number;
        private int monto;
        private String reference;

        public tempdata(int id, String number, int monto) {
            this.number = number;
            this.id = id;
            this.monto = monto;
        }

        public tempdata(int id, String number, int monto, String reference) {
            this.number = number;
            this.id = id;
            this.monto = monto;
            this.reference = reference;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public int getMonto() {
            return monto;
        }

        public void setMonto(int monto) {
            this.monto = monto;
        }

        public String getReference() {
            return reference;
        }

        public void setReference(String comentario) {
            this.reference = comentario;
        }
    }

    /*
    * Conforma el JSON para realizar el envío de una recarga
    * */
    public static String getJSONStringRequest(String token, String action, ArrayList<NumberAdapter.PhoneRequestContainer> phones, Date date, String reference) throws Exception {
        //NUevo squema que de peticon de elementos, dejare el actual abajo adaptado para poder seguir haciendo pruebas.

        Map<String, Object> params = new TreeMap<>();
        params.put("token", token);
        params.put("action", action);
        params.put("date", date.getTime());
//        params.put("reference", reference);

        params.put("reference", ( phones.size() == 1) ? phones.get(0).getReference():"");

        params.put("code", Crypto.getSHA1(token + String.valueOf(date.getTime())));

        List<tempdata> data = new ArrayList<tempdata>();
        int count = 0;
        for (NumberAdapter.PhoneRequestContainer t : phones) {
            //Almaceno el elemento con estado Pendiente
            for (int i = 0; i < t.getCant(); i++) {
                Topup topup = new Topup("+53" + t.getNumber(), "Sin Verificar", "-1", new Date(), (t.getReference() == null || t.getReference().equals(""))?"":t.getReference(), t.getMonto());
                long id = topup.save();
                data.add(new tempdata((int) id, t.getNumber(), t.getMonto(), (t.getReference() == null || t.getReference().equals(""))?"":t.getReference()));
            }
        }
        params.put("phones", data);
        Gson g = new Gson();
        String body = g.toJson(params);
        return body;//message;
    }

    /*
    * Conforma un JSON para el envío de un método sencillo como getBalance
    * */
    public static String getJSONStringRequest(String token, String action) throws Exception {
        Date date = new Date();
        String message = "{\n" +
                "\"token\":\"" + token + "\",\n" +
                "\"action\":\"" + action + "\",\n" +
                "\"date\":" + date.getTime() + ",\n" +
                "\"code\":\"" + Crypto.getSHA1(token + String.valueOf(date.getTime())) + "\"\n" +
                "}";

        return message;
    }

    /*
    * Parsea un string JSON a un objeto ResponseTopUp de la API
    * */
    public static ResponseTopUp topUpFromJSON(String json) {
        Gson GSON = new Gson();
        ResponseTopUp topUp = GSON.fromJson(json, ResponseTopUp.class);
        return topUp;
    }

    public static ResponseBalance balanceFromJSON(String json) {
        Gson GSON = new Gson();
        ResponseBalance balance = GSON.fromJson(json, ResponseBalance.class);
        return balance;
    }

    public static ResponseStatus statusFromJSON(String json) {
        Gson GSON = new Gson();
        ResponseStatus balance = GSON.fromJson(json, ResponseStatus.class);
        return balance;
    }

    public static ResponseCancel cancelFromJSON(String json) {
        Gson GSON = new Gson();
        ResponseCancel balance = GSON.fromJson(json, ResponseCancel.class);
        return balance;
    }
}

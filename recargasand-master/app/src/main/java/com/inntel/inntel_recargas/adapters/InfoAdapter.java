package com.inntel.inntel_recargas.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.joanzapata.iconify.widget.IconTextView;
import com.inntel.inntel_recargas.R;
import com.inntel.inntel_recargas.fragments.NumberPickerDialogFragment;

import java.util.ArrayList;

/**
 * Created by akiel on 5/26/17.
 */

public class InfoAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final ArrayList<String> values;

    public InfoAdapter(Activity context, ArrayList<String> values) {
        super(context, R.layout.topup_item_layout, values);
        this.context = context;
        this.values = values;
    }

    public InfoAdapter getAdapter() {
        return this;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.number_item_layout, parent, false);
        TextView number = (TextView) rowView.findViewById(R.id.textViewNumberItem);
        number.setText(values.get(position));

        if (position % 2 == 1) {
            rowView.setBackgroundColor(Color.parseColor("#EEEEEE"));
        } else {
            rowView.setBackgroundColor(Color.WHITE);
        }

        IconTextView delete = (IconTextView) rowView.findViewById(R.id.deleteNumber);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                values.remove(position);
                getAdapter().notifyDataSetChanged();
            }
        });

        final TextView cantView = (TextView) rowView.findViewById(R.id.cantNumber);
        cantView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                NumberPickerDialogFragment fragment = new NumberPickerDialogFragment();
                fragment.listener = new NumberPickerDialogFragment.OnPickListener() {
                    @Override
                    public void pick(int value) {
                        cantView.setText(value + "");
                    }
                };
                fragment.defaultvalue = Integer.parseInt(cantView.getText().toString());
                fragment.show(context.getFragmentManager(), null);
            }
        });

        return rowView;
    }

//    public class PhoneRequestContainer{
//        private String number;
//        private int cant;
//        private boolean more = false;
//
//        public String getNumber() {
//            return number;
//        }
//
//        public void setNumber(String number) {
//            this.number = number;
//        }
//
//        public int getCant() {
//            return cant;
//        }
//
//        public void setCant(int cant) {
//            this.cant = cant;
//        }
//
//        public boolean isMore() {
//            return more;
//        }
//
//        public void setMore(boolean more) {
//            this.more = more;
//        }
//    }

}

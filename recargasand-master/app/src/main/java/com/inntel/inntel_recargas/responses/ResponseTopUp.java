package com.inntel.inntel_recargas.responses;

/**
 * Created by akiel on 5/28/17.
 */

/*
* Objeto para serializar las respuestas de la API
* */
public class ResponseTopUp {
    String action;
    String reference;
    String error;
    Boolean update;
    Phone[] phones;

    public ResponseTopUp(String action, String reference, String error, Boolean update, Phone[] phones) {
        this.action = action;
        this.reference = reference;
        this.error = error;
        this.update = update;
        this.phones = phones;
    }

    public String getAction() {
        return action;
    }

    public String getReference() {
        return reference;
    }

    public String getError() {
        return error;
    }

    public Boolean getUpdate() {
        return update;
    }

    public Phone[] getPhones() {
        return phones;
    }
}

package com.inntel.inntel_recargas.utils;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Handler;

import com.inntel.inntel_recargas.recivers.NetworkChangeReceiver;

import cu.qva2world.apn.apnOnOff.ApnOnOff;


/**
 * Created by chenry on 19/2/17.
 */
public class ConnectionHelper {

    public static void connect(final Context context, final ConnectListener listener) {

        if (checkWifiConection(context)) {
            if (listener != null) {
                listener.connectedOK(NetworkChangeReceiver.ConnectStatus.WIFI);
            }
            return;
        }


        //Llegados a este punto no se save si la esta conectado a la red movil o no
        if (ApnOnOff.getApnStatus(context)) {
            if (listener != null) {
                listener.connectedOK(NetworkChangeReceiver.ConnectStatus.MOBILE);

            }
            return;
        }

        //Aqui compruebo que el usuario quiere que se le conecte y desconecte automaticamente.
//        if (!Preferences.autoconnect(context)) {
//            if (listener != null) {
//                listener.connectedBLocked();
//            }
//            return;

//        }

        //en este punto si es que el metodo anterior funciona en mi movil, queda claro que no se esta conectado a nada.
        try {

            //se inicia el apn
            ApnOnOff.setApnStatus(context, true);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (ApnOnOff.getApnStatus(context)) {
                        if (listener != null) {
                            listener.connectedOK(NetworkChangeReceiver.ConnectStatus.MOBILE);
                            return;
                        }
                    } else {
                        if (listener != null) {
                            listener.connectedFail(null, "Error no se pudo activar los datos moviles, si su dispositivo tiene una version de android superior a lollipop debe estar rooteado para activarlos automaticamente. Conecte los datos moviles y repita la acción.");
                            return;
                        }
                    }
                }
            }, 4000);

        } catch (Exception e) {
            if (listener != null) {
                listener.connectedFail(e, e.getMessage());
                return;
            }
        }
    }

    public static void disconnect(final Context context, final ConnectListener listener) {

//        if(checkWifiConection(context)){
//            if(listener != null){
//                listener.connectedOK(NetworkChangeReceiver.ConnectStatus.WIFI);
//            }
//            return;
//        }


        //Llegados a este punto no se save si la esta conectado a la red movil o no
        if (!ApnOnOff.getApnStatus(context)) {
            if (listener != null) {
                listener.connectedOK(NetworkChangeReceiver.ConnectStatus.NONE);
                return;
            }
        }

        //en este punto si es que el metodo anterior funciona en mi movil, queda claro que no se esta conectado a nada.
        try {

            //se inicia el apn
            ApnOnOff.setApnStatus(context, false);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!ApnOnOff.getApnStatus(context)) {
                        if (listener != null) {
                            listener.connectedOK(NetworkChangeReceiver.ConnectStatus.NONE);
                            return;
                        }
                    } else {
                        if (listener != null) {
                            listener.connectedFail(null, "Error no se pudo desactivar los datos moviles, si su dispositivo tiene una versión de android superior a lollipop debe estar rooteado para desactivarlos automaticamente. Desonecte los datos moviles manualmente.");
                            return;
                        }
                    }
                }
            }, 4000);

        } catch (Exception e) {
            if (listener != null) {
                listener.connectedFail(e, e.getMessage());
                return;
            }
        }
    }


    public static interface ConnectListener {
        void connectedOK(NetworkChangeReceiver.ConnectStatus status);

        void connectedBLocked();
        void connectedFail(Exception e, String error);
    }

    private static boolean checkWifiConection(Context context) {
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        //Aqui hay que comprobar si la conexion es wifi si hay conexion con el server o smtp o pop o imap segun se elija usar
        return wifi.isWifiEnabled();
    }

    public enum ConnectionStatus {
        DISCONNECT,
        MOBILE,
        WIFI
    }
}

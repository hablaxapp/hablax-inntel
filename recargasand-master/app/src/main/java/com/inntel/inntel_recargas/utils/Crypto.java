package com.inntel.inntel_recargas.utils;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

/**
 * Created by akiel on 5/28/17.
 * (SHA-1) --> http://localhost:8081/stackoverflow.com/26083441
 */

public final class Crypto {
    public static String getSHA1(String s) throws Exception { // Generate Crypto hash
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(s.getBytes("iso-8859-1"), 0, s.length());
        byte[] sha1Hash = md.digest();
        return convertToHex(sha1Hash);
    }

    public static String convertToHex(byte[] data) { // Convert to a HEX string
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String getBase64(String text) throws UnsupportedEncodingException {
        return Base64.encodeToString(text.getBytes("UTF-8"), Base64.DEFAULT);
    }

    public static String decodeBase64(String base64Text) throws UnsupportedEncodingException {
        return new String(Base64.decode(base64Text, Base64.DEFAULT), "UTF-8");
    }
}

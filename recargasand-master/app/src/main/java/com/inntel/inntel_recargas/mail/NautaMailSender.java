package com.inntel.inntel_recargas.mail;

/**
 * Created by akiel on 5/24/17.
 */

import com.inntel.inntel_recargas.utils.Constants;

import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class NautaMailSender extends Authenticator {

    private String username;
    private String password;
    private Session session;

    public NautaMailSender(String username, String password) {
        this.username = username;
        this.password = password;

        if (!this.username.endsWith("@nauta.cu")) {
            this.username += "@nauta.cu";
        }

        Properties props = System.getProperties();
        new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.host", Constants.NAUTA_SMTP);
        props.put("mail.smtp.host", Constants.NAUTA_SMTP);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", Constants.NAUTA_SMTP_PORT);

        props.put("mail.user", this.username);
        props.put("mail.password", this.password);

//        props.put("mail.debug", "true");


        props.put("mail.smtp.timeout", 1000 * 20 + "");
        props.put("mail.smtp.connectiontimeout", 1000 * 20 + "");

        props.put("mail.smtps.timeout", 1000 * 20);
        props.put("mail.smtps.connectiontimeout", 1000 * 20);

        props.put("mail.imap.timeout", 1000 * 20 + "");
        props.put("mail.imap.connectiontimeout", 1000 * 20 + "");

        props.put("mail.imaps.timeout", 1000 * 20);
        props.put("mail.imaps.connectiontimeout", 1000 * 20);


        session = Session.getDefaultInstance(props, this);
    }

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password);
    }

    public synchronized void sendMail(String subject, String body, String recipient) throws MessagingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(username));

        message.setSubject(subject);
        Multipart multipart = new MimeMultipart();
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(body, "text/plain");
        multipart.addBodyPart(messageBodyPart);
        message.setContent(multipart);
        message.setSentDate(new Date());
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
        Transport.send(message);
    }
}


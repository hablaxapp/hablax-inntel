package com.inntel.inntel_recargas.responses;

/**
 * Created by akiel on 5/28/17.
 */

/*
* Representación de un telefono o recarga en ResponseTopUp
* */
public class Phone {
    int id;
    String number;
    private int monto;
    long date;
    String status;
    private String reference;

    public Phone(int id, String number, long date, String status) {
        this.id = id;
        this.number = number;
        this.date = date;
        this.status = status;
    }

    public Phone(int id, String number, long date, String status, String reference) {
        this.id = id;
        this.number = number;
        this.date = date;
        this.status = status;
        this.reference = reference;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getMonto() {
        return monto;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}

/*
 * Copyright (c) 2017.
 */

package com.inntel.inntel_recargas.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.inntel.inntel_recargas.R;

/**
 * Created by chenry on 2/8/17.
 */

public class MontoPickerDialogFragment extends DialogFragment {

    public OnMontoPickListener listener = null;
    public int defaultvalue = 1;

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//
//
//        NumberPicker num = new NumberPicker(getActivity());
//
//        num.setMaxValue(15);
//        num.setMinValue(1);
//        num.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
//            @Override
//            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
//                if (listener != null)
//                    listener.pick(i1);
//            }
//        });
//
//
//        return num;
//    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        Spinner spinMOnto = new Spinner(getActivity());

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getActivity(),
                R.layout.appbar_filter_tittle,
                getActivity().getResources().getStringArray(R.array.monto_value));

        adapter.setDropDownViewResource(R.layout.appbar_filterlist);

        spinMOnto.setAdapter(adapter);

        spinMOnto.setSelection(defaultvalue);

        spinMOnto.setGravity(Gravity.CENTER);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            spinMOnto.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }
        spinMOnto.setPadding(60,10,10,10);


        spinMOnto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (listener != null)
                    listener.pick(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        spinMOnto.setLayoutParams(ActionBar.LayoutParams.);

//        WindowManager.LayoutParams parametros = null;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Escoge monto");
        builder.setView(spinMOnto);

        builder.setPositiveButton("Cerrar", null);

        return builder.create();//super.onCreateDialog(savedInstanceState);
    }

    public interface OnMontoPickListener {
        void pick(int value);
    }
}

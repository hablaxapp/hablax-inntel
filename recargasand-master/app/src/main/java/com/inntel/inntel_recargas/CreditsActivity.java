package com.inntel.inntel_recargas;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.inntel.inntel_recargas.mail.NautaMailReceiver;
import com.inntel.inntel_recargas.mail.NautaMailSender;
import com.inntel.inntel_recargas.responses.ResponseBalance;
import com.inntel.inntel_recargas.utils.Constants;
import com.inntel.inntel_recargas.utils.Crypto;
import com.inntel.inntel_recargas.utils.DataNetwork;
import com.inntel.inntel_recargas.utils.JSONHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class CreditsActivity extends AppCompatActivity {

    private ProgressDialog progessDialog;

    public String nautaUser;
    public String nautaPass;
    public String inntelToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credits);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (DataNetwork.isNetworkConnected(getBaseContext())) {
                    new SenderTask().execute();
                } else {
                    Snackbar.make(view, getString(R.string.InactiveDataSend), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            }).show();
                }

            }
        });

        IconDrawable iconSend = new IconDrawable(this, FontAwesomeIcons.fa_refresh);
        fab.setImageDrawable(iconSend.color(Color.WHITE));

        ActionBar bar = getSupportActionBar();
        bar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_TITLE);
        bar.setHomeButtonEnabled(true);

        loadCredentials();
        loadBalance();
    }

    public void checkCreditsMail() {
//        new NautaReader().execute();
    }

    public void loadCredentials() {
        SharedPreferences preferences = getSharedPreferences("QvaTel_recargas", MODE_PRIVATE);
        nautaUser = preferences.getString("user", "");
        nautaPass = preferences.getString("pass", "");
        inntelToken = preferences.getString("token", "");
    }

    public void loadBalance() {
        TextView textViewCredits = (TextView) findViewById(R.id.textViewCredits);
        TextView textViewDate = (TextView) findViewById(R.id.textViewDateCredits);
        SharedPreferences preferences = getSharedPreferences("QvaTel_recargas", MODE_PRIVATE);
        textViewCredits.setText(String.valueOf(preferences.getInt("balance", 0)));
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");

        if (preferences.getLong("balance_date", 0) > 0) {
            textViewDate.setText(sdf.format(new Date(preferences.getLong("balance_date", 0) * 1000)));
        } else {
            textViewDate.setText("00-00-0000");
        }
    }

    public synchronized void setBalance(int balance, long date) {
        SharedPreferences preferences = getSharedPreferences("QvaTel_recargas", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        if (date > preferences.getLong("balance_date", 0)) {
            editor.putInt("balance", balance);
            editor.putLong("balance_date", date);
        }

        editor.commit();
        loadBalance();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public View getThisView() {
        return getCurrentFocus();
    }

    public Context getThisContext() {
        return this;
    }

    /*
    * Tarea encargada de realizar el envío del correo de recarga
    * */
    private class SenderTask extends AsyncTask {

        private String TAG = "CreditsSenderTask";

        private int errorcode = 1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progessDialog = ProgressDialog.show(getThisContext(), "", getString(R.string.Sending), true);
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            progessDialog.hide();
            if (errorcode == 0) {
                checkCreditsMail();
            }
        }

        @Override
        protected Object doInBackground(Object[] params) {
            NautaMailSender sender = new NautaMailSender(nautaUser, nautaPass);
            try {
                Log.i(TAG, "starting...");
                while (!DataNetwork.isNetworkConnected(getApplication())) {
                    try {
                        Log.i(TAG, "waiting for network... ");
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Log.i(TAG, "network OK. sending mail...");

                String jsonbody = JSONHelper.getJSONStringRequest(inntelToken, "getBalance");
                sender.sendMail("getBalance", Crypto.getBase64(jsonbody), Constants.SERVICE_MAIL);
                Log.i(TAG, "mail sent");
                errorcode = 0;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return errorcode;
        }
    }


    /*
    * Tarea para recibir y parsear los correos de respuesta del servicio
    * */
    private class NautaReader extends AsyncTask {

        private int abalance = 0;
        private long date = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progessDialog = ProgressDialog.show(getThisContext(), "", getString(R.string.Receiving), true);
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            setBalance(abalance, date);
            progessDialog.hide();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            try {
                Thread.sleep(Constants.RECEIVERS_DELAY * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            NautaMailReceiver receiver = new NautaMailReceiver(nautaUser, nautaPass);
            try {
                Map<String, List<String>> mailtexts = receiver.receiveMail("getBalance");

                for (String subject : mailtexts.keySet()) {
                    for (String mailtext : mailtexts.get(subject)) {
                        if (subject.equals("getBalance")) {
                            String msg = Crypto.decodeBase64(mailtext);
                            //ToDo: Manejar los objetos que no se pueden parsear
                            ResponseBalance balace = JSONHelper.balanceFromJSON(msg);

                            if (balace.getError() == null) {
                                if (balace.getDate() > date) {
                                    date = balace.getDate();
                                    abalance = balace.getBalance();
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }
    }
}

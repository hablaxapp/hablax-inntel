package com.inntel.inntel_recargas;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.inntel.inntel_recargas.apis.GeneralApi;
import com.inntel.inntel_recargas.apis.ServiceApi;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.joanzapata.iconify.widget.IconTextView;
import com.orm.SchemaGenerator;
import com.orm.SugarContext;
import com.orm.SugarDb;
import com.inntel.inntel_recargas.adapters.NumberAdapter;
import com.inntel.inntel_recargas.mail.NautaMailSender;
import com.inntel.inntel_recargas.models.Topup;
import com.inntel.inntel_recargas.recivers.NetworkChangeReceiver;
import com.inntel.inntel_recargas.services.ImapPushService;
import com.inntel.inntel_recargas.utils.ConnectionHelper;
import com.inntel.inntel_recargas.utils.Constants;
import com.inntel.inntel_recargas.utils.Crypto;
import com.inntel.inntel_recargas.utils.FileUtils;
import com.inntel.inntel_recargas.utils.JSONHelper;
import com.inntel.inntel_recargas.utils.RegularExpressions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.ConnectException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class RechargeActivity extends AppCompatActivity {

    private static final int RESULT_OPEN_FILE = 6555;
    private static final int PERMISSION_OPEN_FILE = 7555;

    private static final int RESULT_SETTINGS = 6556;

    public ArrayList<NumberAdapter.PhoneRequestContainer> phones = new ArrayList<>();
    public ArrayList<NumberAdapter.PhoneRequestContainer> phonesFromFile = new ArrayList<>();
    public CardView cardViewInfo;
    public EditText etNumber;
    public ListView listViewNumbers;
    public ProgressBar progressBar;

    public static String nautaUser;
    public static String nautaPass;
    public static boolean useImapush;
    public String inntelToken;
    public static String method;
    public String loadedFilename = null;


    private ProgressDialog progessDialog;
    private NumberAdapter adapter;
    private int numberAPICall;


//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//    }
//
//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //inicializa los iconos FA
        Iconify.with(new FontAwesomeModule());

        //inicializa la DDBB y la crea si no existe
        SugarContext.init(getApplication());
        SchemaGenerator schemaGenerator = new SchemaGenerator(this);
        schemaGenerator.createDatabase(new SugarDb(this).getDB());

        setContentView(R.layout.activity_recharge);



        View wrapOfTop = (View) findViewById(R.id.wrap_of_top);
        wrapOfTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
            }
        });
        View screen = (View) findViewById(R.id.wrap_of_body);
        screen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (asking) {
                    return;
                }
                asking = true;

                hideKeyboard();

                ConnectionHelper.connect(RechargeActivity.this, new ConnectionHelper.ConnectListener() {
                    @Override
                    public void connectedOK(NetworkChangeReceiver.ConnectStatus status) {
                        if (!etNumber.getText().equals("") && RegularExpressions.isPhoneNumber(etNumber.getText().toString())) {
                            addPhoneToListClick(null);
//                            phones.add(new NumberAdapter.PhoneRequestContainer(etNumber.getText().toString()));
                        }

                        if (phones.size() > 0 || phonesFromFile.size() > 0) {
                            showSendAlert();
                        } else {
                            Snackbar.make(getCurrentFocus(), getString(R.string.PhoneNeeded), Snackbar.LENGTH_INDEFINITE)
                                    .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                        }
                                    }).show();
                        }

                    }

                    @Override
                    public void connectedBLocked() {
//                        Toast.makeText(MainActivity.this, "Debe estar conectado para poder enviar o recibir.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void connectedFail(Exception e, String error) {
                        Snackbar.make(getCurrentFocus(), getString(R.string.InactiveDataSend), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                    }
                                }).show();
                    }
                });
            }
        });

        IconDrawable iconSend = new IconDrawable(this, FontAwesomeIcons.fa_send);
        fab.setImageDrawable(iconSend.color(Color.WHITE));

        cardViewInfo = (CardView) findViewById(R.id.cardViewInfo);
        etNumber = (EditText) findViewById(R.id.editTextNumber);
        listViewNumbers = (ListView) findViewById(R.id.listViewNumbers);

        loadCredentials();
//        DataNetwork.setMobileDataEnabled(this, true);
        ConnectionHelper.connect(RechargeActivity.this, new ConnectionHelper.ConnectListener() {
            @Override
            public void connectedOK(NetworkChangeReceiver.ConnectStatus status) {
            }

            @Override
            public void connectedBLocked() {
//                        Toast.makeText(MainActivity.this, "Debe estar conectado para poder enviar o recibir.", Toast.LENGTH_LONG).show();
            }

            @Override
            public void connectedFail(Exception e, String error) {
                if (getCurrentFocus() == null) {
                    Toast.makeText(RechargeActivity.this, error, Toast.LENGTH_LONG).show();
                } else
                    Snackbar.make(getCurrentFocus(), error, Snackbar.LENGTH_INDEFINITE)
                            .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            }).show();
            }
        });

        IconTextView iconTextView = (IconTextView) findViewById(R.id.iconTextView);
        iconTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPhoneToListClick(null);
            }
        });
    }

    private boolean findInPhone(String phone) {
        for (NumberAdapter.PhoneRequestContainer p : phones) {
            if (p.getNumber().equals(phone)) {
                p.setCant(p.getCant() + 1);
                return true;
            }
        }
        return false;
    }

    public void comentaryg(View view1) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Comentario general");
        builder.setNegativeButton("Cancelar", null);
        View view = View.inflate(this, R.layout.comentary_layout, null);
        final EditText text = (EditText) view.findViewById(R.id.text);
        text.setText(loadedFilename);
        builder.setView(view);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (text.getText().toString().equals("")) {
                    Toast.makeText(RechargeActivity.this, "Debe especificar un comentario.", Toast.LENGTH_LONG).show();
                    return;
                }
                String test = (text.getText().toString());
                int count = 0;
                if (loadedFilename != null) {
                    //ya lo use una vez por lo que solo actualizo lode que son general

                    for (NumberAdapter.PhoneRequestContainer phone : phones) {
                        if (phone.isGeneral()) {
                            phone.setReference(test);
                            count++;
                        }
                    }
                } else {
                    //es la primera vez qeu lo uso
                    //Aqui lo paso a todos los phones
                    for (NumberAdapter.PhoneRequestContainer phone : phones) {
                        if (phone.getReference() == null || phone.getReference().equals("")) {
                            phone.setGeneral(true);
                            phone.setReference(test);
                            count++;
                        }
                    }
                }
                updateNumbers();
                if (count > 0)
                    loadedFilename = test;
                else
                Toast.makeText(RechargeActivity.this, "No hay elementos a los que ponerle el comentario.", Toast.LENGTH_LONG).show();
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void addPhoneToListClick(View view) {
        if (RegularExpressions.isPhoneNumber(etNumber.getText().toString())) {

            if (!findInPhone(etNumber.getText().toString())) {
                long count = Topup.count(Topup.class, "phone = ? and status != ? and status != ?", new String[]{"+53" + etNumber.getText().toString(), "Sin Verificar", "Fallida"});
                phones.add(0, new NumberAdapter.PhoneRequestContainer(this, etNumber.getText().toString(), count > 0));
            }
            etNumber.setText("");
            updateNumbers();
        } else {
            hideKeyboard();
            Snackbar.make(getCurrentFocus(), getString(R.string.NoNumberLike), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    }).show();
        }
    }

    /*
    * actualiza la lista de números
    * */
    public void updateNumbers() {
        adapter = new NumberAdapter(this, phones);
        listViewNumbers.setAdapter(adapter);
        listViewNumbers.getAdapter().registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                setListViewHeightBasedOnChildren(listViewNumbers);
            }
        });
        setListViewHeightBasedOnChildren(listViewNumbers);
    }

    /*
    * Cambia el tamaño de una lista dinámicamente
    * */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter mAdapter = listView.getAdapter();
        int totalHeight = 0;
        for (int i = 0; i < mAdapter.getCount(); i++) {
            View mView = mAdapter.getView(i, null, listView);
            mView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            totalHeight += mView.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (mAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public void loadCredentials() {
        SharedPreferences preferences = getSharedPreferences("QvaTel_recargas", MODE_PRIVATE);
        nautaUser = preferences.getString("user", "");
        nautaPass = preferences.getString("pass", "");
        inntelToken = preferences.getString("token", "");
        method = preferences.getString("method", "email");
        useImapush = preferences.getBoolean("imapPush", true);

        if (nautaPass.equals("") || nautaUser.equals("") || inntelToken.equals("") || method.equals("")) {
            //si las credenciales o el token estan vacíos vuelve a preguntarlos
            Intent settingsActivity = new Intent(this, SettingsActivity.class);
            startActivityForResult(settingsActivity, RESULT_SETTINGS);
        }
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    Menu currentMenu;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        currentMenu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_recharge, menu);
        menu.findItem(R.id.action_history).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_android)
                        .colorRes(R.color.colorAccent)
                        .actionBarSize());

        menu.findItem(R.id.action_settings).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_wrench)
                        .colorRes(R.color.colorAccent)
                        .actionBarSize());

        menu.findItem(R.id.action_credits).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_dollar)
                        .colorRes(R.color.colorAccent)
                        .actionBarSize());

//

        if (App.useImapush) {
            menu.findItem(R.id.action_ipush).setIcon(
                    new IconDrawable(this, FontAwesomeIcons.fa_volume_off)
                            .colorRes(R.color.colorAccent)
                            .actionBarSize());
            menu.findItem(R.id.action_ipush).setTitle(getString(R.string.action_ipush_off));
        } else {
            menu.findItem(R.id.action_ipush).setIcon(
                    new IconDrawable(this, FontAwesomeIcons.fa_volume_up)
                            .colorRes(R.color.colorAccent)
                            .actionBarSize());
            menu.findItem(R.id.action_ipush).setTitle(getString(R.string.action_ipush_on));
        }
        return true;
    }


    /*
    * Habilitar los icons en el menú si están disponibles
    * */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (menu != null) {
            if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
                try {
                    Method m = menu.getClass().getDeclaredMethod(
                            "setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                } catch (NoSuchMethodException e) {
                } catch (Exception e) {
                }
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settingsActivity = new Intent(this, SettingsActivity.class);
            startActivityForResult(settingsActivity, RESULT_SETTINGS);
            return true;
        } else if (id == R.id.action_history) {
            Intent historyActivity = new Intent(this, HistoryActivity.class);
            startActivity(historyActivity);
            return true;
        } else if (id == R.id.action_credits) {
            Intent creditsActivity = new Intent(this, CreditsActivity.class);
            startActivity(creditsActivity);
            return true;
        } else if (id == R.id.action_acercade) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Acerca de...");
            String str = null;
            try {
                str = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                str = "Version: desconocida";
            }
            builder.setMessage("Versión: " + str);
            builder.setNegativeButton("Cerrar", null);
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        } else if (id == R.id.action_ipush) {
            SharedPreferences preferences = getSharedPreferences("QvaTel_recargas", MODE_PRIVATE);
            if (App.useImapush) {
                App.useImapush = false;
                preferences.edit().putBoolean("imapPush", false).commit();
                ImapPushService.stopAction(this);
                currentMenu.findItem(R.id.action_ipush).setIcon(
                        new IconDrawable(this, FontAwesomeIcons.fa_volume_up)
                                .colorRes(R.color.colorAccent)
                                .actionBarSize());
                currentMenu.findItem(R.id.action_ipush).setTitle(getString(R.string.action_ipush_on));
            } else {
                App.useImapush = true;
                preferences.edit().putBoolean("imapPush", true).commit();
                ImapPushService.startAction(this);
                currentMenu.findItem(R.id.action_ipush).setIcon(
                        new IconDrawable(this, FontAwesomeIcons.fa_volume_off)
                                .colorRes(R.color.colorAccent)
                                .actionBarSize());
                currentMenu.findItem(R.id.action_ipush).setTitle(getString(R.string.action_ipush_off));
            }
        }
        return super.onOptionsItemSelected(item);
    }

    boolean send = false;

    public boolean sending = false;
    public boolean asking = false;

    /*
     * Muestra un diálogo de confirmación antes de realizar una recarga
     **/
    private void showSendAlert() {

        if (sending) {
            Toast.makeText(RechargeActivity.this, "Aun enviando.", Toast.LENGTH_SHORT).show();
            return;
        }
        send = false;
        String msg = null;
        int type = searchAnomaly();

        switch (type) {
            case 0:
                msg = getString(R.string.SendConfirm);
                break;
            case 1:
                msg = "Seguro que desea enviar esta recarga, tenga en cuenta que entre los números que enviará hay algunos <b><font color=\"#fdb418\">repetidos</font></b>.";
                break;
            case 2:
                msg = "Seguro que desea enviar esta recarga, tenga en cuenta que entre los números que enviará hay algunos que ya fueron <b><font color='red'>recargados</font></b>.";
                break;
            case 3:
                msg = "Seguro que desea enviar esta recarga, tenga en cuenta que entre los números que enviará hay algunos que ya fueron <b><font color=\"red\">recargados</font></b> y otros que estan <b><font color=\"#fdb418\">repetidos</font></b>.";
                break;

        }

        View view = getLayoutInflater().inflate(R.layout.advertance, null);
        TextView text = (TextView) view.findViewById(R.id.text);
        CheckBox check = (CheckBox) view.findViewById(R.id.check);
        text.setText(Html.fromHtml(msg));

        check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                send = isChecked;
            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Información");
        builder.setPositiveButton(getString(R.string.Send), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, int i) {
                        asking = false;
                        if (send) {
                            ConnectionHelper.connect(RechargeActivity.this, new ConnectionHelper.ConnectListener() {
                                @Override
                                public void connectedOK(NetworkChangeReceiver.ConnectStatus status) {
                                    if (method.equals("email")) {
                                        new SenderTask().execute();
                                    } else {
                                        final ArrayList<NumberAdapter.PhoneRequestContainer> allphones = new ArrayList<>();
                                        //No se sustituye el phone por el adapter getList ya que la lista se pasa por referencia al adapter por lo quue se actualiza directamente phone
                                        allphones.addAll(phones);
                                        allphones.addAll(phonesFromFile);

                                        if (allphones.size() > 0) {
                                            progessDialog = ProgressDialog.show(get_this_context(), "", getString(R.string.Sending), true);
                                            numberAPICall = 0;
                                            for (int index = 0; index < allphones.size(); index++) {
                                                final NumberAdapter.PhoneRequestContainer phone = allphones.get(index);
                                                numberAPICall += phone.getCant();
                                            }

                                            for (int index = 0; index < allphones.size(); index++) {
                                                final NumberAdapter.PhoneRequestContainer phone = allphones.get(index);
                                                final String number = phone.getNumber();
                                                String amount = String.valueOf(phone.getMonto());
                                                for (int i = 0; i < phone.getCant(); i++) {
                                                    new SenderRechargeTask(amount, number, inntelToken, RechargeActivity.this).execute();

                                                }

                                            }
                                        }
                                        Topup.deleteAll(Topup.class, "status = ?", new String[]{"Sin Verificar"});
                                        dialogInterface.dismiss();
                                    }
                                }

                                    @Override
                                    public void connectedBLocked() {
                                        dialogInterface.dismiss();
                                    }

                                    @Override
                                    public void connectedFail(Exception e, String error) {
                                        if (getCurrentFocus() != null)
                                            Snackbar.make(getCurrentFocus(), getString(R.string.InactiveDataSend), Snackbar.LENGTH_INDEFINITE)
                                                    .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {

                                                        }
                                                    }).show();
                                        dialogInterface.dismiss();
                                    }
                                });


                            }

                        }
                    }).setNegativeButton(getString(R.string.Cancel),
                new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //nothing on cancel
                            asking = false;
                        }
                    });

        builder.setView(view);
                    AlertDialog alertDialog = builder.create();
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            asking = false;
                        }
                    });
        alertDialog.show();


//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setMessage(msg);
//        builder.setCancelable(true)
//                .setPositiveButton(getString(R.string.Send),
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//
//                                new SenderTask().execute();
//                            }
//                        }).setNegativeButton(getString(R.string.Cancel),
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        phones.remove(etNumber.getText().toString());
//                        updateNumbers();
//                    }
//                });
//        AlertDialog alert = builder.create();
//        alert.setTitle(getString(R.string.Send));
//        alert.show();
                }

    /**
     * Busca entre los numeros a agregar si alguno de estos ya fue recargado o si
     * esta duplicada en esta llamada
     *
     * @return
     */
    private int searchAnomaly() {
        for (NumberAdapter.PhoneRequestContainer t : phones) {
            if ((t.isMore()) && (t.getCant() > 1)) {
                return 3;
            }
            if (t.isMore()) return 2;
            if (t.getCant() > 1) return 1;
        }
        return 0;
    }

    /*
    * Pide los permisos para poder leer ficheros y manda a abrir el fichero de los teléfonos
    * con la aplicación que seleccione el usuario
    * */
    public void loadFromFile(View view) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_OPEN_FILE);
        } else {
            openFileDialog();
        }
    }

    public void clickCancelInfo(View view) {
        clearArea();
    }

    public void clickPasteFromClipboard(View view) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

        ClipData clip = clipboard.getPrimaryClip();

        if(clip == null || clip.getItemCount() == 0) {
            Snackbar.make(getCurrentFocus(), getString(R.string.PasteFromClipboardNoPhone), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    }).show();
            return;
        }

        String t = clip.getItemAt(0).getText().toString();

        ArrayList<NumberAdapter.PhoneRequestContainer> clipboarPhones = RegularExpressions.getPhoneNumbers(this, t);
        ClipData setClip = ClipData.newPlainText("", "");
        clipboard.setPrimaryClip(setClip);
        if (clipboarPhones.size() > 0) {
            phones.addAll(clipboarPhones);
            updateNumbers();
        } else {
            hideKeyboard();
            Snackbar.make(getCurrentFocus(), getString(R.string.PasteFromClipboardNoPhone), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    }).show();
        }
    }

    public void clickCleanArea(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.CleanWorkspaceConfirm));
        builder.setCancelable(true)
                .setPositiveButton(getString(R.string.Clean),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                etNumber.setText("");
                                phones.clear();
                                clearArea();
                            }
                        }).setNegativeButton(getString(R.string.Cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //nothing on cancel
                    }
                });

        AlertDialog alert = builder.create();
        alert.setTitle(getString(R.string.Clean));
        alert.show();
    }


    /*
    * Debe dejar el área de trabajo justo como está al iniciar la aplicación
    * Reestablecer aquí todas las variables que cambian con cada envío
    * */
    public void clearArea() {
        cardViewInfo.setVisibility(View.GONE);
//        for (String phone : phonesFromFile){
//            phones.remove(phone);
//        }
        loadedFilename = null;
        phonesFromFile.clear();
        updateNumbers();
    }

    public void openFileDialog() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        startActivityForResult(intent, RESULT_OPEN_FILE);
    }

    /*
    * Lee y parsea el fichero de teléfonos proporcionado por el resultado de openFileDialog()
    * El resultado se almacena en phones y el nombre del fichero en loadedFilename
    * */
    public void openPhonesFile(Intent data) throws URISyntaxException, IOException {
        String path = FileUtils.getPath(this, data.getData());
        phonesFromFile = RegularExpressions.getPhoneNumbers(this, FileUtils.readFile(path));
//        phones.addAll(phonesFromFile);
        TextView numbers = (TextView) findViewById(R.id.textViewNumbers);
        TextView filename = (TextView) findViewById(R.id.textViewFileName);

        numbers.setText(String.valueOf(phonesFromFile.size()));
        loadedFilename = new File(path).getName();
        filename.setText(loadedFilename);

        if (phonesFromFile.size() > 0) {
            cardViewInfo.setVisibility(View.VISIBLE);
            updateNumbers();
        } else {
            Snackbar.make(this.getCurrentFocus(), getString(R.string.NoPhonesInFile), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    }).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_OPEN_FILE) {
            if (data != null) {
                try {
                    openPhonesFile(data);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == RESULT_SETTINGS) {
            loadCredentials();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_OPEN_FILE) {
            openFileDialog();
        }
    }

    public Context get_this_context() {
        return this;
    }



    public class SenderRechargeTask extends AsyncTask<Void, Void, Integer> {
        private String TAG = "SenderTask";

        private Boolean sending;
        public String amount;
        public String number;
        public String token;
        public RechargeActivity rechargeActivity;

        public SenderRechargeTask(String amount, String number, String token, RechargeActivity rechargeActivity) {
            this.amount = amount;
            this.number = number;
            this.token = token;
            this.rechargeActivity = rechargeActivity;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            sending = true;
        }

        @Override
        protected void onPostExecute(Integer errorCode) {
            super.onPostExecute(errorCode);
            sending = false;
            numberAPICall--;
            if (numberAPICall <= 0) {
                progessDialog.hide();
            }

                if (errorCode == 0) { // success
                    List<Topup> list = Topup.find(Topup.class, "status = ?", new String[]{"Sin Verificar"});
                    for (Topup o1 : list) {
                        o1.setStatus("Pendiente");
                        Topup.save(o1);
                    }

                    Snackbar.make(getCurrentFocus(), getString(R.string.SentOK), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            }).show();
                    etNumber.setText("");
                    phones.clear();
                    this.rechargeActivity.clearArea();
                } else {
                    if (errorCode == 116) {
                        Snackbar.make(getCurrentFocus(), getString(R.string.TokenRequired), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // nothing
                                    }
                                }).show();
                    } else if (errorCode == 117) {
                        Snackbar.make(getCurrentFocus(), getString(R.string.TokenInvalid), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // nothing
                                    }
                                }).show();
                    } else if (errorCode == 118) {
                        Snackbar.make(getCurrentFocus(), getString(R.string.SubaccountDisabled), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // nothing
                                    }
                                }).show();
                    } else if (errorCode == 119) {
                        Snackbar.make(getCurrentFocus(), getString(R.string.MainAccountDeactivated), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // nothing
                                    }
                                }).show();
                    } else if (errorCode == 120) {
                        Snackbar.make(getCurrentFocus(), getString(R.string.ParametersOperatorIdAndProductIncorrect), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // nothing
                                    }
                                }).show();
                    } else if (errorCode == 105) {
                        Snackbar.make(getCurrentFocus(), getString(R.string.NumberRequired), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // nothing
                                    }
                                }).show();
                    } else if (errorCode == 109) {
                        Snackbar.make(getCurrentFocus(), getString(R.string.BalanceInsufficientRecharge), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // nothing
                                    }
                                }).show();
                    } else {
                        Snackbar.make(getCurrentFocus(), getString(R.string.SendError), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // nothing
                                    }
                                }).show();
                    }
                }



        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                GeneralApi.ApiResponse response = ServiceApi.apiGetSendRecharges(new GeneralApi.ApiListener() {
                    @Override
                    public void success(GeneralApi.ApiResponse response) {
    //                                sending = false;
    //                                progessDialog.hide();
                    }

                    @Override
                    public void failure(String error) {
    //                                sending = false;
    //                                progessDialog.hide();
                        Snackbar.make(getCurrentFocus(), getString(R.string.SendError), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // nothing
                                    }
                                }).show();
                    }
                }, amount, inntelToken, "53" + number);

                if (response.resultSuccess) {
                    return 0;
                }

                return response.statusCode;
            } catch(Exception e) {
                return 1;
            }
        }
    }

        /*
    * Tarea encargada de realizar el envío del correo de recarga
    * */
    private class SenderTask extends AsyncTask<Void, Void, Integer> {

        private String TAG = "SenderTask";

        private int errorcode = 1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progessDialog = ProgressDialog.show(get_this_context(), "", getString(R.string.Sending), true);
            sending = true;
        }

        @Override
        protected void onPostExecute(Integer o) {
            super.onPostExecute(o);
            sending = false;
            progessDialog.hide();
            if (o == -3) {
                //Elimino todos los sin verificar
//                Topup.deleteAll(Topup.class, "status = ? || status = ?", new String[]{"Sin Verificar", "Pendiente"});
                Topup.deleteAll(Topup.class, "status = ?", new String[]{"Sin Verificar"});
                Snackbar.make(getCurrentFocus(), getString(R.string.SendError), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        }).show();
                return;
            }
            if (o == -2) {
//                Toast.makeText(RechargeActivity.this, "Error de coneccion con el server.", Toast.LENGTH_SHORT).show();
                Topup.deleteAll(Topup.class, "status = ?", new String[]{"Sin Verificar"});
                String num = etNumber.getText().toString();
                recreate();
                etNumber.setText(num);
                return;
            }
            if (o == -1) {
                //Elimino todos los sin verificar
//                Toast.makeText(RechargeActivity.this, "error mas externo Debe haber algun problema con la coneccioón.", Toast.LENGTH_SHORT).show();
                Topup.deleteAll(Topup.class, "status = ?", new String[]{"Sin Verificar"});
                Snackbar.make(getCurrentFocus(), getString(R.string.SendErrorOther), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        }).show();
                return;
            }
            if (errorcode == 0) {
//                Toast.makeText(RechargeActivity.this, "Aqi no hay problemas envio perfecto.", Toast.LENGTH_SHORT).show();
                //Sin Verificar
                //Paso a pendientes todo l que este sin verificar
                List<Topup> list = Topup.find(Topup.class, "status = ?", new String[]{"Sin Verificar"});
                for (Topup o1 : list) {
                    o1.setStatus("Pendiente");
                    Topup.save(o1);
                }

                Snackbar.make(getCurrentFocus(), getString(R.string.SentOK), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        }).show();
                etNumber.setText("");
                phones.clear();
                clearArea();
                return;
            }
//            Toast.makeText(RechargeActivity.this, "Se fue por mal lugar.", Toast.LENGTH_SHORT).show();
            //si no se entra a nada
            try {
                Topup.deleteAll(Topup.class, "status = ?", new String[]{"Sin Verificar"});
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public synchronized int getUnverifiedActions() {
            SharedPreferences preferences = getSharedPreferences("QvaTel_recargas", MODE_PRIVATE);
            return preferences.getInt("unverified_actions", 0);
        }

        public synchronized void setUnverifiedActions(int actions) {
            SharedPreferences preferences = getSharedPreferences("QvaTel_recargas", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("unverified_actions", actions);
            editor.commit();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            NautaMailSender sender = new NautaMailSender(nautaUser, nautaPass);
            try {
                Log.i(TAG, "starting...");
                Log.i(TAG, "network OK. sending mail...");

                final ArrayList<NumberAdapter.PhoneRequestContainer> allphones = new ArrayList<>();
                //No se sustituye el phone por el adapter getList ya que la lista se pasa por referencia al adapter por lo quue se actualiza directamente phone
                allphones.addAll(phones);
                allphones.addAll(phonesFromFile);
//                RechargeActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(RechargeActivity.this, "Cantidad de numeros= " + allphones.size(), Toast.LENGTH_SHORT).show();
//                    }
//                });
                String jsonbody = JSONHelper.getJSONStringRequest(inntelToken, "recharge", allphones, new Date(), "");//loadedFilename
//                RechargeActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(RechargeActivity.this, "Mensaje construido", Toast.LENGTH_SHORT).show();
//                    }
//                });
                try {
                    sender.sendMail("recharge", Crypto.getBase64(jsonbody), Constants.SERVICE_MAIL);
//                    RechargeActivity.this.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(RechargeActivity.this, "Correo enviado", Toast.LENGTH_SHORT).show();
//                        }
//                    });
                } catch (Exception e) {
//                    RechargeActivity.this.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(RechargeActivity.this, "Error en el envio", Toast.LENGTH_SHORT).show();
//                        }
//                    });
                    return -3;
                }
                Log.i(TAG, "mail sent");
                errorcode = 0;

                setUnverifiedActions(getUnverifiedActions() + 1);
                return 0;

            } catch (ConnectException e) {
                if (e.getMessage().contains("localhost")) {
                    return -2;
                }
                e.printStackTrace();
                return -1;

            } catch (Exception e) {
                e.printStackTrace();
                return -1;
            }
        }
    }

}

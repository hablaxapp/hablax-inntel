package com.inntel.inntel_recargas;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

public class SettingsActivity extends AppCompatActivity {

    SharedPreferences preferences;
    EditText nautaUser;
    EditText nautaPass;
    EditText inntelToken;
    Switch useEmail;
    Switch useInternet;
    CardView cardViewNauta;

    final CompoundButton.OnCheckedChangeListener useEmailListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            useInternet.setOnCheckedChangeListener(null);
            useInternet.setChecked(!b);
            useInternet.setOnCheckedChangeListener(useInternetListener);
            cardViewNauta.setVisibility(View.VISIBLE);
        }
    };

    final CompoundButton.OnCheckedChangeListener useInternetListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            useEmail.setOnCheckedChangeListener(null);
            useEmail.setChecked(!b);
            useEmail.setOnCheckedChangeListener(useEmailListener);
            cardViewNauta.setVisibility(View.GONE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        nautaUser = (EditText) findViewById(R.id.editTextUser);
        nautaPass = (EditText) findViewById(R.id.editTextPassword);
        inntelToken = (EditText) findViewById(R.id.editTextToken);

        cardViewNauta = (CardView) findViewById(R.id.cardViewNauta);


        useEmail = (Switch) findViewById(R.id.textUseEmail);
        useEmail.setOnCheckedChangeListener(useEmailListener);


        useInternet = (Switch) findViewById(R.id.textUseInternet);
        useInternet.setOnCheckedChangeListener(useInternetListener);

        TextView textLink = (TextView) findViewById(R.id.textLink);
        textLink.setMovementMethod(LinkMovementMethod.getInstance());

        preferences = getSharedPreferences("QvaTel_recargas", MODE_PRIVATE);
        String additionalNauta = preferences.getString("user", "").equals("") ? "" : "@nauta.cu";
        nautaUser.setText(preferences.getString("user", "").equals(" ") ? "" : preferences.getString("user", "") + additionalNauta);
        nautaPass.setText(preferences.getString("pass", "").equals(" ") ? "" : preferences.getString("pass", ""));
        inntelToken.setText(preferences.getString("token", ""));
        String method = preferences.getString("method", "email");
        if (method.equals("email")) {
            useEmail.setChecked(true);
            useInternet.setChecked(false);
            cardViewNauta.setVisibility(View.VISIBLE);
        } else {
            useEmail.setChecked(false);
            useInternet.setChecked(true);
            cardViewNauta.setVisibility(View.GONE);
        }
//        inntelToken.setText("0a7b10df144b4c401af77027");

        ActionBar bar = getSupportActionBar();
        bar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_TITLE);
        bar.setHomeButtonEnabled(true);


        Spinner spiner = (Spinner) findViewById(R.id.spin);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                R.layout.appbar_filter_tittle,
                getResources().getStringArray(R.array.monto_value));

        adapter.setDropDownViewResource(R.layout.appbar_filterlist);

        spiner.setAdapter(adapter);

        spiner.setSelection(preferences.getInt("monto_defecto", 1));

        spiner.setGravity(Gravity.CENTER);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            spiner.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }
//        spinMOnto.setPadding(60,10,10,10);

        spiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                preferences.edit().putInt("monto_defecto", position).commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void clickSave(View view) {
        String methodUse = useEmail.isChecked() ? "email" : "internet";
        if (methodUse.equals("internet")) {
            if (inntelToken.getText().toString().equals("")) {
                Snackbar.make(view, getString(R.string.FillRequiredFields), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        }).show();
                return;
            }
            nautaUser.setText(" ");
            nautaPass.setText(" ");
        } else {
            if (nautaUser.getText().toString().equals("") ||
                    nautaPass.getText().toString().equals("") ||
                    inntelToken.getText().toString().equals("")) {
                Snackbar.make(view, getString(R.string.FillRequiredFields), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        }).show();
                return;
            }

            if (nautaUser.getText().toString().indexOf("@nauta.cu") == -1) {
                Snackbar.make(view, getString(R.string.UsernameShouldBeNauta), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.Dismiss), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        }).show();
                return;
            }
        }

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("user", nautaUser.getText().toString().replace("@nauta.cu", ""));
        editor.putString("pass", nautaPass.getText().toString());
        editor.putString("token", inntelToken.getText().toString());
        editor.putString("method", useEmail.isChecked() ? "email" : "internet");
        editor.commit();


        App.nautaUser = nautaUser.getText().toString().replace("@nauta.cu", "");
        App.nautaPass = nautaPass.getText().toString();
        App.inntelToken = inntelToken.getText().toString();
        App.method = useEmail.isChecked() ? "email" : "internet";

        setResult(1);
        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(1);
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.inntel.inntel_recargas.recivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.inntel.inntel_recargas.App;
import com.inntel.inntel_recargas.services.ImapPushService;


/**
 * Created by chenry on 17/2/17.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {
    public static String repoURL = "http://qvacall.qva2world.com";
//    public static String repoURL = "http://192.168.43.63";

    public int requestNotificationCode = 11040;
    public static boolean inprogress = false;

    public static ConnectStatus STATUS = ConnectStatus.NONE;

    public static enum ConnectStatus {
        NONE, WIFI, MOBILE
    }

    /**/
    @Override
    public void onReceive(final Context context, Intent intent) {
        boolean isWifiConnected = false;
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);

            if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
                if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                    if (STATUS != ConnectStatus.MOBILE)
                        STATUS = ConnectStatus.WIFI;
                } else if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                    STATUS = ConnectStatus.MOBILE;
                }
//                if (Preferences.useImapPush(context) && !ImapPushService.running) {
                if(App.useImapush)
                    ImapPushService.startAction(context);
//                }
            } else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTING) {
                //Aqui tengo que ver que ecambioes hay que ahacer aqui

            } else if (networkInfo != null && (networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED || networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTING)) {
                STATUS = ConnectStatus.NONE;
//                ImapPushService.stopAction(context);
            }
        }
    }
}
